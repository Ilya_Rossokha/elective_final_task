<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:choose>
    <c:when test="${locale == 'ru'}">
        <fmt:setLocale value="ru"/>
    </c:when>
    <c:otherwise>
        <fmt:setLocale value="en"/>
    </c:otherwise>
</c:choose>
<fmt:setBundle basename="local"/>

<%@ attribute name="path" required="true" %>
<%@ attribute name="maxPage" type="java.lang.Integer" required="true" %>
<%@ attribute name="page" type="java.lang.Integer" required="true" %>


<fmt:message key="page.previous" var="prev"/>
<c:choose>
    <c:when test="${page == 1}">${prev}</c:when>
    <c:otherwise>
        <a href="${path}?page=${page-1}">${prev}</a>
    </c:otherwise>
</c:choose>

<fmt:message key="page.next" var="next"/>
<c:choose>
    <c:when test="${page == maxPage}">${next}</c:when>
    <c:otherwise>
        <a href="${path}?page=${page+1}">${next}</a>
    </c:otherwise>
</c:choose>
