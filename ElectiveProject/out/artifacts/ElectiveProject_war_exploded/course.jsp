<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<c:choose>
    <c:when test="${locale == 'ru'}">
        <fmt:setLocale value="ru"/>
    </c:when>
    <c:otherwise>
        <fmt:setLocale value="en"/>
    </c:otherwise>
</c:choose>
<fmt:setBundle basename="local"/>
<html>
<head>
    <title>${course.description}</title>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW"
            crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Bitter:400,700">
    <link rel="stylesheet" href="assets/css/style.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../styles/course.css">
    <link rel='icon' type='image/x-icon' href='../../image/course.ico' />
</head>
<body>
<%@include file="header.jspf"%>
<div class="masthead">
    <div class="container">
        <form  action="/course" method="post">
        <div class="col-md-6 col-sm-12">
            <h1>${course.description}</h1>
            <fmt:message key="user.page_theme" var="theme"/>
            <h3>${theme} ${course.themeName}</h3>
            <c:set var="role" value="${sessionScope['role']}"/>
            <c:choose>
                <c:when test="${role == 'TEACHER'}">
                    <fmt:message key="user.page_cannot_register" var="c_reg"/>
                    <button class="btn btn-danger" name="action" value="register" disabled>${c_reg}</button>
                </c:when>
                <c:when test="${requestScope.isregistered == 0}">
                    <fmt:message key="user.page_register" var="reg"/>
                    <button class="btn btn-success" name="action" value="register">${reg}</button>
                </c:when>
                <c:otherwise>
                    <fmt:message key="user.page_already_registered" var="a_reg"/>
                    <button class="btn btn-success" name="action" value="register" disabled>${a_reg}</button>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
    <div class="container">
        <div class="row">

            <div class="col-md-4 col-sm-4">
                <div class="feature-thumb">
                    <div class="se">
                        <span>01</span>
                        <fmt:message key="user.page_duration" var="duration"/>
                        <h3>${duration}</h3>
                        <p>${course.duration}</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-4">
                <div class="feature-thumb">
                    <span>02</span>
                    <fmt:message key="user.page_teacher" var="teacher"/>
                    <h3>${teacher}</h3>
                    <form  action="course" method="post">
                        <p>${course.teacherName}</p>
                    </form>
                </div>
            </div>

            <div class="col-md-4 col-sm-4">
                <div class="feature-thumb">
                    <span>03</span>
                    <fmt:message key="user.page_start_date" var="sdate"/>
                    <h3>${sdate}</h3>
                    <p>${course.start_date}</p>
                </div>
            </div>

        </div>
    </form>
    </div>
</div>
</body>
</html>
