<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<c:choose>
    <c:when test="${locale == 'ru'}">
        <fmt:setLocale value="ru"/>
    </c:when>
    <c:otherwise>
        <fmt:setLocale value="en"/>
    </c:otherwise>
</c:choose>
<fmt:setBundle basename="local"/>

<html>
<head>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
    <title>Profile Page</title>
    <link rel='shortcut icon' type='image/x-icon' href='../../image/profile.ico' />
    <link rel="stylesheet" type="text/css" href="../../styles/profile.css">
</head>
<body>
<%@include file="header.jspf"%>

<div class="main">
<c:set var="ongoing" value="${requestScope['list_ongoing_courses']}"/>
<c:set var="soon" value="${requestScope['list_soon_courses']}"/>
<c:set var="finished" value="${requestScope['list_finished_courses']}"/>
<div class="row">
    <div class="col-4">
        <div class="list-group" id="list-tab" role="tablist">
            <fmt:message key="profile.page_home" var="home"/>
            <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">${home}</a>
            <c:if test="${not empty ongoing}">
                <fmt:message key="profile.page_ongoing" var="ong"/>
                <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">${ong}</a>
            </c:if>
            <c:if test="${not empty finished}">
                <fmt:message key="profile.page_finished" var="finish"/>
                <a class="list-group-item list-group-item-action" id="list-messages-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages">${finish}</a>
            </c:if>
            <c:if test="${not empty soon}">
                <fmt:message key="profile.page_soon" var="soonstart"/>
                <a class="list-group-item list-group-item-action" id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings">${soonstart}</a>
            </c:if>
        </div>
    </div>
    <div class="col-8">
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
                <c:set var="user" value="${sessionScope['loguser']}"/>
                <c:set var="info" value="${sessionScope['info']}"/>
                <div class="col-md-8">
                    <div class="card mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-3">
                                    <fmt:message key="profile.page_login" var="login"/>
                                    <h6 class="mb-0">${login}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    ${user.login}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <fmt:message key="profile.page_firstname" var="fn"/>
                                    <h6 class="mb-0">${fn}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    ${info.firstname}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <fmt:message key="profile.page_surname" var="sn"/>
                                    <h6 class="mb-0">${sn}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    ${info.surname}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <fmt:message key="profile.page_email" var="em"/>
                                    <h6 class="mb-0">${em}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    ${user.email}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="btnmar">
                                    <fmt:message key="profile.page_change_password" var="cpass"/>
                                <a href="/edit" class="btn-sm btn-secondary" role="button">${cpass}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
                <h1>${ong}</h1>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <fmt:message key="profile.page_ongoing_page_end_date" var="ed"/>
                        <th scope="col">${ed}</th>
                        <fmt:message key="profile.page_description" var="desc"/>
                        <th scope="col">${desc}</th>
                        <fmt:message key="profile.page_theme" var="th"/>
                        <th scope="col">${th}</th>
                        <fmt:message key="profile.page_teacher" var="teach"/>
                        <th scope="col">${teach}</th>
                    </tr>
                    </thead>
                    <c:forEach items="${list_ongoing_courses}" var="course">
                        <tbody>
                        <tr>
                            <td><c:out value="${course.end_date}"/></td>
                            <td><c:out value="${course.description}"/></td>
                            <td><c:out value="${course.themeName}"/></td>
                            <td><c:out value="${course.teacherName}"/></td>
                        </tr>
                        </tbody>
                    </c:forEach>
                </table>
            </div>
            <div class="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-messages-list">
                <h1>${finish}</h1>
                <table class="table table-bordered">
                <thead>
                    <tr>
                        <fmt:message key="profile.page_description" var="desc"/>
                        <th scope="col">${desc}</th>
                        <fmt:message key="profile.page_theme" var="th"/>
                        <th scope="col">${th}</th>
                        <fmt:message key="profile.page_teacher" var="teach"/>
                        <th scope="col">${teach}</th>
                        <fmt:message key="profile.page_grade" var="grade"/>
                        <th scope="col">${grade}</th>
                    </tr>
                </thead>
                    <c:forEach items="${list_finished_courses}" var="course">
                    <tbody>
                        <tr>
                            <td><c:out value="${course.description}"/></td>
                            <td><c:out value="${course.themeName}"/></td>
                            <td><c:out value="${course.teacherName}"/></td>
                            <c:set var="role" value="${sessionScope['role']}"/>
                            <c:choose>
                            <c:when test="${role == 'TEACHER'}">
                                <form action="/gradebook" method="get">
                                    <td>
                                        <fmt:message key="profile.page_gradebook" var="gb"/>
                                        <button class="btn btn-success" type="submit" name="gradebook" value="<c:out value="${course.id}"/>">${gb} №<c:out value="${course.id}"/>
                                        </button>
                                    </td>
                                </form>
                            </c:when>
                            <c:otherwise>
                                <td>
                                    <div class="align-span"><span><c:out value="${course.grade}"/></span>
                                    </div>
                                </td>
                            </c:otherwise>
                            </c:choose>

                        </tr>
                    </tbody>
                    </c:forEach>
                </table>
            </div>
            <div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list">
                <h1>${soonstart}</h1>
                <table class="table table-bordered">
                <thead>
                    <tr>
                        <fmt:message key="profile.page_soon_page_start_date" var="starts"/>
                        <th scope="col">${starts}</th>
                        <fmt:message key="profile.page_description" var="desc"/>
                        <th scope="col">${desc}</th>
                        <fmt:message key="profile.page_theme" var="th"/>
                        <th scope="col">${th}</th>
                        <fmt:message key="profile.page_teacher" var="teach"/>
                        <th scope="col">${teach}</th>
                        <fmt:message key="profile.page_duration" var="dur"/>
                        <th scope="col">${dur}</th>
                    </tr>
                </thead>
                    <c:forEach items="${list_soon_courses}" var="course">
                    <tbody>
                        <tr>
                            <td><c:out value="${course.start_date}"/></td>
                            <td><c:out value="${course.description}"/></td>
                            <td><c:out value="${course.themeName}"/></td>
                            <td><c:out value="${course.teacherName}"/></td>
                            <td><c:out value="${course.duration}"/></td>
                        </tr>
                    </tbody>
                    </c:forEach>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>