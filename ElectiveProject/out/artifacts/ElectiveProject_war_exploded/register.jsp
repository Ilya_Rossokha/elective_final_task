<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<c:choose>
    <c:when test="${locale == 'ru'}">
        <fmt:setLocale value="ru"/>
    </c:when>
    <c:otherwise>
        <fmt:setLocale value="en"/>
    </c:otherwise>
</c:choose>
<fmt:setBundle basename="local"/>
<html>
<head>
    <title>Registration Page</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../styles/register.css">
    <link rel='icon' type='image/x-icon' href='../../image/reglog.ico'/>
</head>
<body>
<jsp:include page="header2.jspf"/>
<div class="signup-form">
    <form action="/register" method="post">
        <fmt:message key="registration.registration_form.submit" var="subm"/>
        <h2>${subm}</h2>
        <fmt:message key="registration.registration_text" var="text"/>
        <p>${text}</p>
        <hr>
        <c:set var="message" value="${requestScope['errorMessage']}"/>
        <c:if test="${not empty message}">
            <p class="errormes">${message}</p>
        </c:if>
        <div class="form-group">
            <div  class="input-group">
                <div class="input-group-prepend">
					<span class="input-group-text">
						<span class="fa fa-user" aria-hidden="true"></span>
					</span>
                </div>
                <fmt:message key="registration.registration_form.login" var="login"/>
                <input type="text" class="form-control" name="login" placeholder="${login}" required="required">
            </div>
        </div>
        <div class="form-group">
            <div  class="input-group">
                <div class="input-group-prepend">
					<span class="input-group-text">
						<span class="fa fa-user" aria-hidden="true"></span>
					</span>
                </div>
                <fmt:message key="registration.registration_form.firstname" var="firstname"/>
                <input type="text" class="form-control" name="firstname" placeholder="${firstname}" required="required">
            </div>
        </div>
        <div class="form-group">
            <div  class="input-group">
                <div class="input-group-prepend">
					<span class="input-group-text">
						<span class="fa fa-user" aria-hidden="true"></span>
					</span>
                </div>
                <fmt:message key="registration.registration_form.secondname" var="secondname"/>
                <input type="text" class="form-control" name="surname" placeholder="${secondname}" required="required">
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-prepend">
					<span class="input-group-text">
						<i class="fa fa-paper-plane"></i>
					</span>
                </div>
                <fmt:message key="registration.registration_form.email" var="email"/>
                <input type="email" class="form-control" name="email" placeholder="${email}" required="required">
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-prepend">
					<span class="input-group-text">
						<i class="fa fa-lock"></i>
					</span>
                </div>
                <fmt:message key="registration.registration_form.password" var="password"/>
                <input type="password" class="form-control" name="password" placeholder="${password}" required="required">
            </div>
        </div>
        <div class="form-group">
            <fmt:message key="local.reg_ref" var="signUp"/>
            <button type="submit" class="btn btn-primary btn-lg">${signUp}</button>
        </div>
    </form>
    <fmt:message key="registration.registration_form.authentication.if_registered" var="if_registered"/>
    <fmt:message key="registration.registration_form.authentication.log_in" var="log_in"/>
    <div class="text-center">${if_registered} <a href="/login">${log_in}</a></div>
</div>
</body>
</html>

