<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<c:choose>
    <c:when test="${locale == 'ru'}">
        <fmt:setLocale value="ru"/>
    </c:when>
    <c:otherwise>
        <fmt:setLocale value="en"/>
    </c:otherwise>
</c:choose>
<fmt:setBundle basename="local"/>
<html>
<head>
    <title>Create Teacher</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../../styles/register.css">
    <link rel='icon' type='image/x-icon' href='../../image/admin.ico' />
</head>
<body>
<jsp:include page="header2.jspf"/>
<div class="signup-form">
    <form action="/teacher" method="post">
        <h2>New Teacher</h2>
        <p>Please fill this to add new teacher</p>
        <hr>
        <div class="form-group">
            <div  class="input-group">
                <div class="input-group-prepend">
					<span class="input-group-text">
						<span class="fa fa-user" aria-hidden="true"></span>
					</span>
                </div>
                <fmt:message key="registration.registration_form.login" var="login"/>
                <input type="text" class="form-control input-lg" name="login" placeholder="${login}" required="required">
            </div>
        </div>
        <div class="form-group">
            <div  class="input-group">
                <div class="input-group-prepend">
					<span class="input-group-text">
						<span class="fa fa-user" aria-hidden="true"></span>
					</span>
                </div>
                <fmt:message key="registration.registration_form.firstname" var="firstname"/>
                <input type="text" class="form-control input-lg" name="firstname" placeholder="${firstname}" required="required">
            </div>
        </div>
        <div class="form-group">
            <div  class="input-group">
                <div class="input-group-prepend">
					<span class="input-group-text">
						<span class="fa fa-user" aria-hidden="true"></span>
					</span>
                </div>
                <fmt:message key="registration.registration_form.secondname" var="secondname"/>
                <input type="text" class="form-control input-lg" name="surname" placeholder="${secondname}" required="required">
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-prepend">
					<span class="input-group-text">
						<i class="fa fa-paper-plane"></i>
					</span>
                </div>
                <fmt:message key="registration.registration_form.email" var="email"/>
                <input type="email" class="form-control input-lg" name="email" placeholder="${email}" required="required">
            </div>
        </div>
        <div class="form-group">
            <fmt:message key="local.reg_ref" var="signUp"/>
            <button type="submit" class="btn btn-success btn-lg btn-block">${signUp}</button>
        </div>
    </form>

</div>
</body>
</html>

