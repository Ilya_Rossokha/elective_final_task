package epam.com.DAO;

import epam.com.model.*;
import org.apache.log4j.Logger;

import java.sql.*;


public class AccountDAO {
    private static final Logger LOG = Logger.getLogger(AccountDAO.class);


    public static Account InsertAccount(Account account) {
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.INSERT_ACCOUNT, Statement.RETURN_GENERATED_KEYS)) {
                    connection.setAutoCommit(false);
                    preparedStatement.setString(1, account.getLogin());
                    preparedStatement.setString(2, account.getPassword());
                    preparedStatement.setString(3, account.getEmail());
                    preparedStatement.setInt(4, account.getRole());
                    preparedStatement.setBoolean(5, account.isActive());
                    preparedStatement.executeUpdate();
                    ResultSet rs = preparedStatement.getGeneratedKeys();
                    if (rs.next()) {
                        account.setId(rs.getInt(1));
                    }
                } catch (SQLException ex) {
                    LOG.error(ex.getMessage());
                    ConnectionPool.rollbackAndClose(connection);
                } finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getMessage());
        }
        return account;
    }

    public static void deleteAccount(Account account) {
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.DELETE_ACCOUNT_BY_LOGIN)) {
                    connection.setAutoCommit(false);
                    preparedStatement.setString(1, account.getLogin());
                    preparedStatement.executeUpdate();
                } catch (SQLException ex) {
                    LOG.error(ex.getLocalizedMessage());
                    ConnectionPool.rollbackAndClose(connection);
                } finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getLocalizedMessage());
        }
    }

    public static Account getAccount(String login) {
        Account account = new Account();
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.GET_ACCOUNT_BY_LOGIN)) {
                    connection.setAutoCommit(false);
                    preparedStatement.setString(1, login);
                    preparedStatement.execute();
                    ResultSet rs = preparedStatement.getResultSet();
                    while (rs.next()) {
                        account.setLogin(rs.getString("login"));
                        account.setEmail(rs.getString("email"));
                        account.setId(rs.getInt("id"));
                        account.setPassword(rs.getString("password"));
                        account.setRole(rs.getInt("role"));
                        account.setActive(rs.getBoolean("active"));
                    }
                    rs.close();
                } catch (SQLException ex) {
                    LOG.error(ex.getLocalizedMessage());
                    ConnectionPool.rollbackAndClose(connection);
                } finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getLocalizedMessage());
        }
        return account;
    }


    public static void updateActive(int id, int active) {
        String UPDATE_ACTIVE = "UPDATE account SET active = " + active + " WHERE id = ?";
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_ACTIVE)) {
                    connection.setAutoCommit(false);
                    preparedStatement.setInt(1, id);
                    preparedStatement.executeUpdate();
                } catch (SQLException ex) {
                    LOG.error(ex.getLocalizedMessage());
                    ConnectionPool.rollbackAndClose(connection);
                } finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getLocalizedMessage());

        }
    }

    public static void updatePassword(Account account) {
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.UPDATE_PASSWORD_BY_ID)) {
                    connection.setAutoCommit(false);
                    preparedStatement.setString(1, account.getPassword());
                    preparedStatement.setInt(2, account.getId());
                    preparedStatement.executeUpdate();
                } catch (SQLException ex) {
                    LOG.error(ex.getLocalizedMessage());
                    ConnectionPool.rollbackAndClose(connection);
                } finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getLocalizedMessage());

        }
    }

    public static Integer isStudentRegisteredToCourse(Student student, Course course) {
        int result = -1;
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.IS_STUDENT_REGISTERED_TO_COURSE)) {
                    connection.setAutoCommit(false);
                    preparedStatement.setInt(1, student.getId());
                    preparedStatement.setInt(2, course.getId());
                    preparedStatement.execute();
                    ResultSet rs = preparedStatement.getResultSet();
                    while (rs.next()) {
                        result = rs.getInt(1);
                    }
                    rs.close();
                } catch (SQLException ex) {
                    LOG.error(ex.getLocalizedMessage());
                    ConnectionPool.rollbackAndClose(connection);
                } finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getLocalizedMessage());
        }
        return result;
    }

    public static Integer isEmailExist(String email) {
        int result = 0;
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.IS_EMAIL_EXIST)) {
                    connection.setAutoCommit(false);
                    preparedStatement.setString(1, email);
                    preparedStatement.execute();
                    ResultSet rs = preparedStatement.getResultSet();
                    while (rs.next()) {
                        result = rs.getInt(1);
                    }
                    rs.close();
                } catch (SQLException ex) {
                    LOG.error(ex.getLocalizedMessage());
                    ConnectionPool.rollbackAndClose(connection);
                } finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getLocalizedMessage());
        }
        return result;
    }

    public static Integer isLoginExist(String login) {
        int result = 0;
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.IS_LOGIN_EXIST)) {
                    connection.setAutoCommit(false);
                    preparedStatement.setString(1, login);
                    preparedStatement.execute();
                    ResultSet rs = preparedStatement.getResultSet();
                    while (rs.next()) {
                        result = rs.getInt(1);
                    }
                    rs.close();
                } catch (SQLException ex) {
                    LOG.error(ex.getLocalizedMessage());
                    ConnectionPool.rollbackAndClose(connection);
                } finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getLocalizedMessage());
        }
        return result;
    }
}
