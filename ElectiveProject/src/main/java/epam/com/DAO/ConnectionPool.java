package epam.com.DAO;

import org.apache.tomcat.jdbc.pool.PoolProperties;


import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionPool {

    private ConnectionPool(){
    }

    /**
     * Establish connection with MySQL
     *
     * @return Connection with MySQL
     */

    public static synchronized Connection getConnection() {
        try {
            PoolProperties p = new PoolProperties();
            p.setUrl("jdbc:mysql://localhost:3306/" + "elective_db?serverTimezone=UTC");
            p.setDriverClassName("com.mysql.cj.jdbc.Driver");
            p.setMaxActive(100);
            p.setMaxIdle(30);
            p.setMaxWait(10000);
            p.setUsername("root");
            p.setPassword("83!YsPUSSwZ#Wglh4p");
            DataSource datasource = new org.apache.tomcat.jdbc.pool.DataSource(p);
            return datasource.getConnection();
        } catch (SQLException ex) {
           return null;
        }
    }

    public static void commitAndClose(Connection con) {
        try {
            con.commit();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Rollbacks and close the given connection.
     *
     * @param con
     *            Connection to be rollbacked and closed.
     */
    public static void rollbackAndClose(Connection con) {
        try {
            con.rollback();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

}
