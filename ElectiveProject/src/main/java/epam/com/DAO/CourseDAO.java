package epam.com.DAO;

import epam.com.model.Course;
import epam.com.model.Account;
import epam.com.model.Student;
import org.apache.log4j.Logger;



import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class CourseDAO {

    private static final Logger LOG = Logger.getLogger(CourseDAO.class);

    public static Course getCourseById(int id) {
        Course course = new Course();
        String query = "SELECT course.id, course.description, start_date, end_date, theme.name, teacher.surname, teacher.firstname FROM course INNER JOIN theme ON course.theme_id = theme.id INNER JOIN teacher ON course.teacher_id = teacher.id  WHERE course.id = ?";
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.GET_COURSE_BY_ID)) {
                    connection.setAutoCommit(false);
                    preparedStatement.setInt(1, id);
                    preparedStatement.execute();
                    ResultSet rs = preparedStatement.getResultSet();
                    while (rs.next()) {
                        course.setId(rs.getInt("id"));
                        course.setDescription(rs.getString("description"));
                        course.setThemeName(rs.getString("name"));
                        String surname = rs.getString("surname");
                        String firstname = rs.getString("firstname");
                        course.setTeacherName(firstname + " " + surname);
                        course.setStart_date(rs.getDate("start_date"));
                        course.setEnd_date(rs.getDate("end_date"));
                        course.setDuration(rs.getDate("start_date"), rs.getDate("end_date"));
                        course.setProgress(course.progress(rs.getDate("start_date"), rs.getDate("end_date")));
                    }
                    rs.close();
                } catch (SQLException ex) {
                    LOG.error(ex.getLocalizedMessage());
                    ConnectionPool.rollbackAndClose(connection);
                } finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getLocalizedMessage());
        }
        return course;
    }


    public static void setCourseForUser(Student student, Course course) {
        String query = "INSERT INTO student_of_course(student_id, course_id) VALUES (?, ?)";
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.STUDENT_REGISTER_TO_COURSE)) {
                    connection.setAutoCommit(false);
                    preparedStatement.setInt(1, student.getId());
                    preparedStatement.setInt(2, course.getId());
                    preparedStatement.executeUpdate();
                } catch (SQLException ex) {
                    LOG.error(ex.getLocalizedMessage());
                    ConnectionPool.rollbackAndClose(connection);
                } finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getLocalizedMessage());
        }
    }

    public static int getCoursesNumber() {
        int count = 0;
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.COUNT_COURSE_NUMBER)) {
                    connection.setAutoCommit(false);
                    preparedStatement.execute();
                    ResultSet rs = preparedStatement.getResultSet();
                    while (rs.next()) {
                        count = rs.getInt(1);
                    }
                    rs.close();
                } catch (SQLException ex) {
                    LOG.error(ex.getLocalizedMessage());
                    ConnectionPool.rollbackAndClose(connection);
                } finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getLocalizedMessage());
        }
        return count;
    }

    public static List<Course> getCourses(int page) {
        List<Course> listCourses = new ArrayList<>();
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.GET_COURSES_BY_PAGE)) {
                    connection.setAutoCommit(false);
                    preparedStatement.setInt(1, 10);
                    preparedStatement.setInt(2, 10 * (page - 1));
                    preparedStatement.execute();
                    ResultSet rs = preparedStatement.getResultSet();
                    while (rs.next()) {
                        Course course = new Course();
                        course.setId(rs.getInt("id"));
                        course.setDescription(rs.getString("description"));
                        course.setThemeName(rs.getString("name"));
                        String surname = rs.getString("surname");
                        String firstname = rs.getString("firstname");
                        course.setTeacherName(firstname + " " + surname);
                        course.setCount(rs.getInt("count"));
                        course.setStart_date(rs.getDate("start_date"));
                        course.setEnd_date(rs.getDate("end_date"));
                        course.setNumDuration(rs.getDate("start_date"), rs.getDate("end_date"));
                        course.setDuration(rs.getDate("start_date"), rs.getDate("end_date"));
                        course.setProgress(course.progress(rs.getDate("start_date"), rs.getDate("end_date")));
                        listCourses.add(course);
                    }
                    rs.close();
                } catch (SQLException ex) {
                    LOG.error(ex.getLocalizedMessage());
                    ConnectionPool.rollbackAndClose(connection);
                } finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getLocalizedMessage());
        }

        return listCourses;
    }

    public static List<Course> getCourses() {
        List<Course> listCourses = new ArrayList<>();
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.GET_ALL_COURSES)) {
                    connection.setAutoCommit(false);
                    preparedStatement.execute();
                    ResultSet rs = preparedStatement.getResultSet();
                    while (rs.next()) {
                        Course course = new Course();
                        course.setId(rs.getInt("id"));
                        course.setDescription(rs.getString("description"));
                        course.setThemeName(rs.getString("name"));
                        String surname = rs.getString("surname");
                        String firstname = rs.getString("firstname");
                        course.setTeacherName(firstname + " " + surname);
                        course.setCount(rs.getInt("count"));
                        course.setStart_date(rs.getDate("start_date"));
                        course.setEnd_date(rs.getDate("end_date"));
                        course.setNumDuration(rs.getDate("start_date"), rs.getDate("end_date"));
                        course.setDuration(rs.getDate("start_date"), rs.getDate("end_date"));
                        course.setProgress(course.progress(rs.getDate("start_date"), rs.getDate("end_date")));
                        listCourses.add(course);
                    }
                    rs.close();
                } catch (SQLException ex) {
                    LOG.error(ex.getLocalizedMessage());
                    ConnectionPool.rollbackAndClose(connection);
                } finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getLocalizedMessage());
        }
        return listCourses;
    }

    public static List<Course> getStudentCourses(Account account) {
        List<Course> listCourses = new ArrayList<>();
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.GET_ALL_COURSES_OF_STUDENT)) {
                    connection.setAutoCommit(false);
                    preparedStatement.setString(1, account.getLogin());
                    preparedStatement.execute();
                    ResultSet rs = preparedStatement.getResultSet();
                    while (rs.next()) {
                        Course course = new Course();
                        course.setId(rs.getInt("id"));
                        course.setDescription(rs.getString("description"));
                        course.setThemeName(rs.getString("name"));
                        String surname = rs.getString("surname");
                        String firstname = rs.getString("firstname");
                        course.setTeacherName(surname + " " + firstname);
                        course.setStart_date(rs.getDate("start_date"));
                        course.setEnd_date(rs.getDate("end_date"));
                        course.setDuration(rs.getDate("start_date"), rs.getDate("end_date"));
                        course.setProgress(course.progress(rs.getDate("start_date"), rs.getDate("end_date")));
                        course.setGrade(rs.getInt("grade"));
                        listCourses.add(course);
                    }
                    rs.close();
                } catch (SQLException ex) {
                    LOG.error(ex.getLocalizedMessage());
                    ConnectionPool.rollbackAndClose(connection);
                } finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getLocalizedMessage());
        }
        return listCourses;
    }

    public static List<Course> getTeacherCourses(Account account) {
        List<Course> listCourses = new ArrayList<>();
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.GET_ALL_COURSES_OF_TEACHER)) {
                    connection.setAutoCommit(false);
                    preparedStatement.setString(1, account.getLogin());
                    preparedStatement.execute();
                    ResultSet rs = preparedStatement.getResultSet();
                    while (rs.next()) {
                        Course course = new Course();
                        course.setId(rs.getInt("id"));
                        course.setDescription(rs.getString("description"));
                        course.setThemeName(rs.getString("name"));
                        String surname = rs.getString("surname");
                        String firstname = rs.getString("firstname");
                        course.setTeacherName(surname + " " + firstname);
                        course.setStart_date(rs.getDate("start_date"));
                        course.setEnd_date(rs.getDate("end_date"));
                        course.setDuration(rs.getDate("start_date"), rs.getDate("end_date"));
                        course.setProgress(course.progress(rs.getDate("start_date"), rs.getDate("end_date")));
                        listCourses.add(course);
                    }
                    rs.close();

                } catch (SQLException ex) {
                    LOG.error(ex.getLocalizedMessage());
                    ConnectionPool.rollbackAndClose(connection);
                } finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getLocalizedMessage());
        }
        return listCourses;
    }

    public static void InsertCourse(Course course) {
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.INSERT_COURSE, Statement.RETURN_GENERATED_KEYS)) {
                    connection.setAutoCommit(false);
                    preparedStatement.setString(1, course.getDescription());
                    preparedStatement.setInt(2, Integer.parseInt(course.getTeacherName()));
                    preparedStatement.setInt(3, Integer.parseInt(course.getThemeName()));
                    preparedStatement.setDate(4, course.getStart_date());
                    preparedStatement.setDate(5, course.getEnd_date());
                    preparedStatement.executeUpdate();
                    ResultSet rs = preparedStatement.getGeneratedKeys();
                    if (rs.next()) {
                        course.setId(rs.getInt(1));
                    }
                } catch (SQLException ex) {
                    LOG.error(ex.getLocalizedMessage());
                    ConnectionPool.rollbackAndClose(connection);
                } finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getLocalizedMessage());
        }
    }

    public static void updateCourseById(Course course) {
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.UPDATE_COURSE_BY_ID)) {
                    connection.setAutoCommit(false);
                    preparedStatement.setString(1, course.getDescription());
                    preparedStatement.setInt(2, ThemeDAO.getThemeByName(course.getThemeName()).getId());
                    preparedStatement.setInt(3, TeacherDAO.getTeacherByFullName(course.getTeacherName()).getId());
                    preparedStatement.setDate(4, course.getStart_date());
                    preparedStatement.setDate(5, course.getEnd_date());
                    preparedStatement.setInt(6, course.getId());
                    preparedStatement.executeUpdate();
                } catch (SQLException ex) {
                    LOG.error(ex.getLocalizedMessage());
                    ConnectionPool.rollbackAndClose(connection);
                } finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getLocalizedMessage());
        }
    }

    public static void deleteCourse(Course course) {
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.DELETE_COURSE_BY_ID, Statement.RETURN_GENERATED_KEYS)) {
                    connection.setAutoCommit(false);
                    preparedStatement.setInt(1, course.getId());
                    preparedStatement.executeUpdate();
                } catch (SQLException ex) {
                    LOG.error(ex.getLocalizedMessage());
                    ConnectionPool.rollbackAndClose(connection);
                } finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getLocalizedMessage());
        }
    }



}
