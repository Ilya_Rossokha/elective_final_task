package epam.com.DAO;

public class Query {

   public static final String INSERT_STUDENT = "INSERT INTO student (account_id, firstname, surname) VALUES (?,?,?)";
   public static final String INSERT_TEACHER = "INSERT INTO teacher (account_id, firstname, surname) VALUES (?,?,?)";
   public static final String INSERT_ACCOUNT = "INSERT INTO account (login,password,email,role,active) VALUES (?,?,?,?,?)";
   public static final String GET_ACCOUNT_BY_LOGIN = "SELECT * FROM account WHERE login=?";
   public static final String DELETE_ACCOUNT_BY_LOGIN = "DELETE FROM account WHERE login = ?";
   public static final String GET_STUDENT_BY_ACCOUNT_ID = "SELECT student.id, student.account_id, student.firstname, student.surname FROM account INNER JOIN student on student.account_id = account.id WHERE account.id = ?";
   public static final String IS_STUDENT_REGISTERED_TO_COURSE = "SELECT EXISTS(SELECT student_of_course.student_id, student_of_course.course_id FROM student_of_course WHERE student_of_course.student_id = ? AND student_of_course.course_id=?);";
   public static final String IS_LOGIN_EXIST = "SELECT EXISTS(SELECT account.login FROM account WHERE account.login = ?)";
   public static final String IS_EMAIL_EXIST = "SELECT EXISTS(SELECT account.email FROM account WHERE account.email = ?)";
   public static final String GET_COURSE_BY_ID = "SELECT course.id, course.description, start_date, end_date, theme.name, teacher.surname, teacher.firstname FROM course INNER JOIN theme ON course.theme_id = theme.id INNER JOIN teacher ON course.teacher_id = teacher.id  WHERE course.id = ?";
   public static final String UPDATE_PASSWORD_BY_ID = "UPDATE account  SET password=? WHERE account.id=?";
   public static final String GET_COURSE_BY_ID_WITH_PARAMETER = "SELECT course.id, course.description, start_date, end_date, theme.name, teacher.surname, teacher.firstname FROM course INNER JOIN theme ON course.theme_id = theme.id INNER JOIN teacher ON course.teacher_id = teacher.id  WHERE course.id = ?";
   public static final String STUDENT_REGISTER_TO_COURSE = "INSERT INTO student_of_course(student_id, course_id) VALUES (?, ?)";

   public static final String COUNT_STUDENT_NUMBER = "select count(*) from student";
   public static final String COUNT_COURSE_NUMBER = "select count(*) from course WHERE course.start_date >= CURDATE()";
   public static final String GET_COURSES_BY_PAGE = "SELECT course.id, description,start_date, end_date, name, surname, firstname, count(student_of_course.student_id) as COUNT FROM course INNER JOIN theme ON theme.id=course.theme_id INNER JOIN teacher ON teacher.id=course.teacher_id LEFT JOIN student_of_course ON course.id= student_of_course.course_id WHERE start_date >= CURDATE()  GROUP BY id limit ? offset ?";

   public static final String GET_ALL_COURSES = "SELECT course.id, description,start_date, end_date, name, surname, firstname, count(student_of_course.student_id) as COUNT FROM course INNER JOIN theme ON theme.id=course.theme_id INNER JOIN teacher ON teacher.id=course.teacher_id LEFT JOIN student_of_course ON course.id= student_of_course.course_id WHERE start_date >= CURDATE() GROUP BY id";

   public static final String GET_ALL_COURSES_OF_STUDENT = "SELECT course.id, course.description, start_date, end_date, theme.name, teacher.surname, teacher.firstname, student_of_course.grade FROM student INNER JOIN student_of_course ON student_of_course.student_id=student.id INNER JOIN course ON student_of_course.course_id=course.id INNER JOIN theme ON course.theme_id = theme.id INNER JOIN teacher ON course.teacher_id = teacher.id INNER JOIN account ON account.id = student.account_id WHERE account.login = ?";

   public static final String GET_ALL_COURSES_OF_TEACHER = "SELECT course.id, course.description, start_date, end_date, theme.name, teacher.surname, teacher.firstname FROM course INNER JOIN theme ON course.theme_id = theme.id INNER JOIN teacher ON course.teacher_id = teacher.id INNER JOIN account ON account.id = teacher.account_id WHERE account.login = ?";

   public static final String INSERT_COURSE = "INSERT INTO course (description,teacher_id,theme_id,start_date,end_date) VALUES (?,?,?,?,?)";

   public static final String UPDATE_COURSE_BY_ID = "UPDATE COURSE  SET description=?, theme_id=?,teacher_id=?,start_date=?,end_date=? WHERE course.id=?";

   public static final String DELETE_COURSE_BY_ID = "DELETE FROM course WHERE id = ?";


   public static final String SELECT_ACTIVE_FIELD_OF_USER = "SELECT account.active FROM account INNER JOIN student ON student.account_id = account.id WHERE student.id = ?";
   public static final String GET_ALL_STUDENTS_BY_PAGE = "SELECT student.id, account_id,firstname,surname,active FROM student JOIN account ON account.id = student.account_id limit ? offset ?";
   public static final String GET_ALL_STUDENTS_ON_COURSE = "SELECT grade, student.id,student.account_id, student.firstname, student.surname FROM student_of_course JOIN student ON student.id = student_of_course.student_id WHERE student_of_course.course_id = ?";
   public static final String UPDATE_GRADE_OF_STUDENT = "UPDATE student_of_course SET grade = ? WHERE student_id = ? AND course_id = ?";
   public static final String GET_ALL_TEACHERS = "SELECT * FROM teacher";

   public static final String GET_TEACHER_BY_FULLNAME = "SELECT firstname, surname, account_id, id FROM teacher WHERE teacher.firstname = ? AND teacher.surname = ?;";
   public static final String GET_TEACHER_BY_ACCOUNT_ID = "SELECT teacher.id, teacher.account_id, teacher.firstname, teacher.surname FROM account INNER JOIN teacher on teacher.account_id = account.id WHERE account.id = ?";

   public static final String GET_THEME_BY_NAME = "SELECT id, name from THEME WHERE theme.name = ?";
   public static final String INSERT_THEME = "INSERT INTO THEME (name) VALUES (?)";
   public static final String DELETE_THEME = "DELETE FROM theme WHERE name = ?";
   public static final String GET_ALL_THEMES = "SELECT * FROM theme;";



}
