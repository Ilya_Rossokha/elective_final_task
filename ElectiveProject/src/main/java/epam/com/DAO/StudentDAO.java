package epam.com.DAO;

import epam.com.model.Account;
import epam.com.model.Student;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class StudentDAO {
    private static final Logger LOG = Logger.getLogger(StudentDAO.class);

    public static void insertStudent(Student student) {
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.INSERT_STUDENT, Statement.RETURN_GENERATED_KEYS)) {
                    connection.setAutoCommit(false);
                    preparedStatement.setInt(1, student.getAccount_id());
                    preparedStatement.setString(2, student.getFirstname());
                    preparedStatement.setString(3, student.getSurname());
                    preparedStatement.executeUpdate();
                } catch (SQLException ex) {
                    LOG.error(ex.getLocalizedMessage());
                    ConnectionPool.rollbackAndClose(connection);
                }
                finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getLocalizedMessage());
        }
    }



    public static List<Student> getAllStudents(int page) {
        List<Student> studentList = new ArrayList<>();
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.GET_ALL_STUDENTS_BY_PAGE )) {
                    connection.setAutoCommit(false);
                    preparedStatement.setInt(1, 10);
                    preparedStatement.setInt(2, 10 * (page - 1));
                    preparedStatement.execute();
                    preparedStatement.execute();
                    ResultSet rs = preparedStatement.getResultSet();
                    while (rs.next()) {
                        Student student = new Student();
                        student.setId(rs.getInt("id"));
                        student.setAccount_id(rs.getInt("account_id"));
                        student.setFirstname(rs.getString("firstname"));
                        student.setSurname(rs.getString("surname"));
                        student.setIsBlocked(rs.getInt("active"));
                        studentList.add(student);
                    }
                    rs.close();
                }  catch (SQLException ex) {
                    LOG.error(ex.getLocalizedMessage());
                    ConnectionPool.rollbackAndClose(connection);
                }
                finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getLocalizedMessage());
        }
        return studentList;
    }
    public static int getStudensNumber() {
        int count = 0;
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.COUNT_STUDENT_NUMBER)) {
                    connection.setAutoCommit(false);
                    preparedStatement.execute();
                    ResultSet rs = preparedStatement.getResultSet();
                    while (rs.next()) {
                        count = rs.getInt(1);
                    }
                    rs.close();
                } catch (SQLException ex) {
                    LOG.error(ex.getLocalizedMessage());
                    ConnectionPool.rollbackAndClose(connection);
                } finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getLocalizedMessage());
        }
        return count;
    }
    public static Student getStudentByAccountId(Account account) {
        Student student = new Student();
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.GET_STUDENT_BY_ACCOUNT_ID)) {
                    connection.setAutoCommit(false);
                    preparedStatement.setInt(1, account.getId());
                    preparedStatement.execute();
                    ResultSet rs = preparedStatement.getResultSet();
                    while (rs.next()) {
                        student.setId(rs.getInt("id"));
                        student.setAccount_id(rs.getInt("account_id"));
                        student.setFirstname(rs.getString("firstname"));
                        student.setSurname(rs.getString("surname"));
                    }
                    rs.close();
                } catch (SQLException ex) {
                    LOG.error(ex.getLocalizedMessage());
                    ConnectionPool.rollbackAndClose(connection);
                }
                finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getLocalizedMessage());

        }
        return student;
    }

}
