package epam.com.DAO;

import epam.com.model.*;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class StudentOfCourseDAO {
    private static final Logger LOG = Logger.getLogger(StudentOfCourseDAO.class);

    public static List<Student> getAllStudentsOnCourse(Course course) {
        List<Student> studentList = new ArrayList<>();
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.GET_ALL_STUDENTS_ON_COURSE)) {
                    connection.setAutoCommit(false);
                    preparedStatement.setInt(1, course.getId());
                    preparedStatement.execute();
                    ResultSet rs = preparedStatement.getResultSet();
                    while (rs.next()) {
                        Student student = new Student();
                        student.setId(rs.getInt("id"));
                        student.setFirstname(rs.getString("firstname"));
                        student.setSurname(rs.getString("surname"));
                        student.setGrade(rs.getInt("grade"));
                        studentList.add(student);
                    }
                    rs.close();
                } catch (SQLException ex) {
                    LOG.error(ex.getLocalizedMessage());
                    ConnectionPool.rollbackAndClose(connection);
                }
                finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getLocalizedMessage());
        }
        return studentList;
    }

    public static void updateStudentGrade(Student student, Course course) {
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.UPDATE_GRADE_OF_STUDENT)) {
                    connection.setAutoCommit(false);
                    preparedStatement.setInt(1, student.getGrade());
                    preparedStatement.setInt(2, student.getId());
                    preparedStatement.setInt(3, course.getId());
                    preparedStatement.executeUpdate();
                } catch (SQLException ex) {
                    LOG.error(ex.getLocalizedMessage());
                    ConnectionPool.rollbackAndClose(connection);
                }
                finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getLocalizedMessage());
        }
    }

}

