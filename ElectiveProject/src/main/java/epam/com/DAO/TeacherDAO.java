package epam.com.DAO;

import epam.com.Util.PasswordService;
import epam.com.model.Account;
import epam.com.model.Teacher;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class TeacherDAO {
    private static final Logger LOG = Logger.getLogger(TeacherDAO.class);

    public static void insertTeacher(Teacher teacher) {
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.INSERT_TEACHER, Statement.RETURN_GENERATED_KEYS)) {
                    connection.setAutoCommit(false);
                    preparedStatement.setInt(1, teacher.getAccount_id());
                    preparedStatement.setString(2, teacher.getFirstname());
                    preparedStatement.setString(3, teacher.getSurname());
                    preparedStatement.executeUpdate();
                } catch (SQLException ex) {
                    LOG.error(ex.getMessage());
                    ConnectionPool.rollbackAndClose(connection);
                } finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getLocalizedMessage());
        }
    }

    public static List<Teacher> getAllTeacher() {
        List<Teacher> teacherList = new ArrayList<>();
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.GET_ALL_TEACHERS)) {
                    connection.setAutoCommit(false);
                    preparedStatement.execute();
                    ResultSet rs = preparedStatement.getResultSet();
                    while (rs.next()) {
                        Teacher teacher = new Teacher();
                        teacher.setId(rs.getInt("id"));
                        teacher.setAccount_id(rs.getInt("account_id"));
                        teacher.setFirstname(rs.getString("firstname"));
                        teacher.setSurname(rs.getString("surname"));
                        teacher.setFullname(rs.getString("firstname") + " " + rs.getString("surname"));
                        teacherList.add(teacher);
                    }
                    rs.close();
                } catch (SQLException ex) {
                    LOG.error(ex.getMessage());
                    ConnectionPool.rollbackAndClose(connection);
                }
                finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getMessage());
        }
        return teacherList;
    }

    public static Teacher getTeacherByFullName(String fullname) {
        Teacher teacher = new Teacher();
        String[] fullArr = fullname.split(" ");
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.GET_TEACHER_BY_FULLNAME)) {
                    connection.setAutoCommit(false);
                    preparedStatement.setString(1, fullArr[0]);
                    preparedStatement.setString(2, fullArr[1]);
                    preparedStatement.execute();
                    ResultSet rs = preparedStatement.getResultSet();
                    while (rs.next()) {
                        teacher.setId(rs.getInt("id"));
                        teacher.setAccount_id(rs.getInt("account_id"));
                        teacher.setFirstname(rs.getString("firstname"));
                        teacher.setSurname(rs.getString("surname"));
                        teacher.setFullname(rs.getString("firstname") + " " + rs.getString("surname"));
                    }
                    rs.close();
                } catch (SQLException ex) {
                    LOG.error(ex.getMessage());
                    ConnectionPool.rollbackAndClose(connection);
                }
                finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getMessage());
        }
        return teacher;
    }
    public static Teacher getTeacherByAccountId(Account account) {
        Teacher teacher = new Teacher();
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.GET_TEACHER_BY_ACCOUNT_ID)) {
                    connection.setAutoCommit(false);
                    preparedStatement.setInt(1, account.getId());
                    preparedStatement.execute();
                    ResultSet rs = preparedStatement.getResultSet();
                    while (rs.next()) {
                        teacher.setId(rs.getInt("id"));
                        teacher.setAccount_id(rs.getInt("account_id"));
                        teacher.setFirstname(rs.getString("firstname"));
                        teacher.setSurname(rs.getString("surname"));
                    }
                    rs.close();
                } catch (SQLException ex) {
                    LOG.error(ex.getMessage());
                    ConnectionPool.rollbackAndClose(connection);
                }
                finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getMessage());

        }
        return teacher;
    }
}
