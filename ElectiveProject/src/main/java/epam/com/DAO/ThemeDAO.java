package epam.com.DAO;

import epam.com.model.Theme;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class ThemeDAO {
    private static final Logger LOG = Logger.getLogger(ThemeDAO.class);

    public static List<Theme> getAllThemes() {
        List<Theme> themes = new ArrayList<>();
        String GET_ALL_THEMES = "SELECT * FROM theme;";
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.GET_ALL_THEMES)) {
                    connection.setAutoCommit(false);
                    preparedStatement.execute();
                    ResultSet rs = preparedStatement.getResultSet();
                    while (rs.next()) {
                        Theme theme = new Theme();
                        theme.setId(rs.getInt("id"));
                        theme.setName(rs.getString("name"));
                        themes.add(theme);
                    }
                    rs.close();
                    connection.commit();
                } catch (SQLException ex) {
                    LOG.error(ex.getLocalizedMessage());
                    ConnectionPool.rollbackAndClose(connection);
                }
                finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getLocalizedMessage());
        }
        return themes;
    }


    public static Theme getThemeByName(String name) {
        Theme theme = new Theme();
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.GET_THEME_BY_NAME)) {
                    connection.setAutoCommit(false);
                    preparedStatement.setString(1, name);
                    preparedStatement.execute();
                    ResultSet rs = preparedStatement.getResultSet();
                    while (rs.next()) {
                        theme.setId(rs.getInt("id"));
                        theme.setName(rs.getString("name"));
                    }
                    rs.close();
                } catch (SQLException ex) {
                    LOG.error(ex.getLocalizedMessage());
                    ConnectionPool.rollbackAndClose(connection);
                }
                finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getLocalizedMessage());
        }
        return theme;
    }

    public static void insertTheme(String name) {
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.INSERT_THEME)) {
                    connection.setAutoCommit(false);
                    preparedStatement.setString(1, name);
                    preparedStatement.execute();
                } catch (SQLException ex) {
                    LOG.error(ex.getLocalizedMessage());
                    ConnectionPool.rollbackAndClose(connection);
                }
                finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getLocalizedMessage());
        }
    }

    public static void deleteTheme(String name) {
        try (Connection connection = ConnectionPool.getConnection()) {
            if (connection != null) {
                try (PreparedStatement preparedStatement = connection.prepareStatement(Query.DELETE_THEME)) {
                    connection.setAutoCommit(false);
                    preparedStatement.setString(1, name);
                    preparedStatement.executeUpdate();
                } catch (SQLException ex) {
                    LOG.error(ex.getLocalizedMessage());
                    ConnectionPool.rollbackAndClose(connection);
                }
                finally {
                    ConnectionPool.commitAndClose(connection);
                }
            }
        } catch (SQLException ex) {
            LOG.error(ex.getLocalizedMessage());
        }
    }
}
