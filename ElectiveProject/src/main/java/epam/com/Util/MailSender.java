package epam.com.Util;

import epam.com.DAO.AccountDAO;
import epam.com.model.Account;
import org.apache.log4j.Logger;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;
import java.util.Properties;

public class MailSender {
    private static final Logger LOG = Logger.getLogger(MailSender.class);
    /**
     * Sending email for verify account for students
     * for teachers sending generated password
     * user's courses
     * @param account
     * @param code
     */
    public static void sendMail(Account account, String code) {
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", 465);
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", 465);
        Session session = Session.getInstance(props,
                new Authenticator() {
                    public PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("coursealex23", "coursemainsender22");
                    }
                });
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("coursealex23@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(account.getEmail()));
            message.setSubject("EPAM FACULTY");
            String text = "Welcome to EPAM FACULTY! " +
                    "\nPlease find your login below." +
                    "\nYour login: " + account.getLogin() +
                    "\nYour generated password: " + account.getPassword() +
                    "\nYou can change password in your profile page" +
                    "\nBest regards!";

            if(account.getRole()==0){
                text = "Please verify your account using this code: " + code;
            }
            message.setContent(text, "text/html; charset=utf-8");

            Transport.send(message);
            LOG.trace("Mail sent to " + account.getEmail());
        } catch (MessagingException ex) {
            ex.printStackTrace();
        }

    }


}
