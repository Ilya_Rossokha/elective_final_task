package epam.com.Util;

import com.google.common.hash.Hashing;
import org.apache.commons.lang3.RandomStringUtils;

import java.nio.charset.StandardCharsets;

public class PasswordService {

    public static String generatePassword(){
        return RandomStringUtils.random(8, "ABCDEFGHIJKLMNOPQRSTUVWXYZ-1234567890");
    }

    public static String hashPassword(String password){
        return Hashing.sha256().hashString(password, StandardCharsets.UTF_8).toString();
    }


}
