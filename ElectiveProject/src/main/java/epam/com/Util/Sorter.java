package epam.com.Util;

import epam.com.DAO.CourseDAO;
import epam.com.model.Course;

import java.util.ArrayList;
import java.util.List;

public class Sorter {


    public List <Course> getCourseByPage(int page, List <Course> courseList){
        List <Course> newList = new ArrayList<>();
        int down = page*10-10;
        if(page*10 > courseList.size()){
            for (int i = down; i<courseList.size();i++){
                newList.add(courseList.get(i));
            }
        }
        else {
            for (int i = down; i < page * 10; i++) {
                newList.add(courseList.get(i));
            }
        }
        return newList;
    }

    public List <Course> getCoursesByTheme(String theme, List<Course> courseList){
        List <Course> newList = new ArrayList<>();
        for (Course course: courseList){
            if(course.getThemeName().contains(theme)){
                newList.add(course);
            }
        }
        return newList;
    }

    public List <Course> getCoursesByTeacher(String teacher, List<Course> courseList){
        List <Course> newList = new ArrayList<>();
        for (Course course: courseList){
            if(course.getTeacherName().contains(teacher)){
                newList.add(course);
            }
        }
        return newList;
    }

    public  List <Course> getProgressOfCourses(String progress, List<Course> courseList){
        List <Course> newList = new ArrayList<>();
        for (Course course: courseList){
            if(course.toString().contains(progress)){
                newList.add(course);
            }
        }
        return newList;
    }

}
