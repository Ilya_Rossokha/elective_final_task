package epam.com.controller;

import epam.com.DAO.AccountDAO;
import epam.com.DAO.CourseDAO;
import epam.com.model.Course;
import epam.com.model.Account;
import epam.com.model.Student;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/course")
public class CourseServlet extends javax.servlet.http.HttpServlet {
    private static final Logger LOG = Logger.getLogger(CourseServlet.class);

    /**
     * Register student to course
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(javax.servlet.http.HttpServletRequest request,
                          javax.servlet.http.HttpServletResponse response) throws ServletException, IOException {

        LOG.trace("START TRACING COURSE SERVLET");
        String action = request.getParameter("action");
        if ("register".equals(action)) {
            HttpSession session = request.getSession();

            Course course = (Course) session.getAttribute("course");
            Student student = (Student) session.getAttribute("info");

            CourseDAO.setCourseForUser(student, course);
            response.sendRedirect(request.getRequestURI() + "?id=" + course.getId());
        }
    }

    /**
     * Display course page
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(javax.servlet.http.HttpServletRequest request,
                         javax.servlet.http.HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("loguser")!=null) {
            Course course = CourseDAO.getCourseById(Integer.parseInt(request.getParameter("id")));
            if(session.getAttribute("info").getClass() == Student.class) {
                Student student = (Student) session.getAttribute("info");
                request.setAttribute("isregistered", AccountDAO.isStudentRegisteredToCourse(student, course));
            }
            request.setAttribute("course", course);
            session.setAttribute("course", course);
            request.getRequestDispatcher("/course.jsp").forward(request, response);
        }
        else {
            response.sendRedirect("/login");
        }
    }
}

