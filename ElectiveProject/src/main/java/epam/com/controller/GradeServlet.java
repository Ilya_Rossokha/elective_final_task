package epam.com.controller;

import epam.com.DAO.CourseDAO;
import epam.com.DAO.StudentOfCourseDAO;
import epam.com.model.Course;
import epam.com.model.Student;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/gradebook")
public class GradeServlet extends javax.servlet.http.HttpServlet  {
    private static final Logger LOG = Logger.getLogger(GradeServlet.class);

    /**
     * Teacher update grade book of course
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(javax.servlet.http.HttpServletRequest request,
                         javax.servlet.http.HttpServletResponse response)
            throws ServletException, IOException {
        LOG.trace("START TRACING GRADE SERVLET");
        HttpSession session = request.getSession();
        Course course = (Course) session.getAttribute("crs");
        System.out.println(course);
        List <Student> studentList = (List<Student>) session.getAttribute("students");
        System.out.println(studentList);
        String[] grades = request.getParameterValues("grade");
        for (int i=0; i<studentList.size();i++){
            System.out.println("STUDENT IN POST = "+studentList.get(i) + " GRADE IN POST = " + grades[i]);
            if(studentList.get(i).getGrade()!=Integer.parseInt(grades[i])){
                studentList.get(i).setGrade(Integer.parseInt(grades[i]));
                StudentOfCourseDAO.updateStudentGrade(studentList.get(i),course);
            }
        }

        response.sendRedirect("/profile");
    }

    /**
     * Display grade book for course
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(javax.servlet.http.HttpServletRequest request,
                         javax.servlet.http.HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Course course = CourseDAO.getCourseById(Integer.parseInt(String.valueOf(request.getParameter("gradebook"))));
        System.out.println("course form GET"+course);
        session.setAttribute("crs", course);
        List<Student> list_students = StudentOfCourseDAO.getAllStudentsOnCourse(course);
        System.out.println("LIST STUDENT GET " + list_students);
        request.setAttribute("list_students",list_students);
        session.setAttribute("students", list_students);
        RequestDispatcher dispatcher = request.getRequestDispatcher("gradebook.jsp");
        dispatcher.forward(request, response);
    }
}
