package epam.com.controller;

import epam.com.DAO.AccountDAO;
import epam.com.DAO.StudentDAO;
import epam.com.DAO.TeacherDAO;
import epam.com.Util.VerifyCaptcha;
import epam.com.model.Account;
import epam.com.model.Role;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;
import java.util.Objects;
import java.util.ResourceBundle;

import static org.w3c.dom.bootstrap.DOMImplementationRegistry.PROPERTY;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(LoginServlet.class);

    /**
     * Log in if input data is correct
     * create session, set attribute as account object
     * redirect to main page
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(javax.servlet.http.HttpServletRequest request,
                          javax.servlet.http.HttpServletResponse response)
            throws ServletException, IOException {
        LOG.trace("START TRACING LOGIN SERVLET");
        HttpSession session = request.getSession();

        Locale locale = (Locale) request.getSession().getAttribute("locale");
        ResourceBundle resourceBundle = ResourceBundle.getBundle("local", locale);



        String login = request.getParameter("login");
        String password = request.getParameter("password");

        //error handler
        String errorMessage;
        boolean valid = true;

        if (login == null || password == null || login.isEmpty() || password.isEmpty()) {
            errorMessage = "Login/password cannot be empty";
            request.setAttribute("errorMessage", errorMessage);
        }


        Account account = AccountDAO.getAccount(login);

        System.out.println(account);
        LOG.info("User " + login + " logged");

        if (account.getLogin() == null || !Objects.equals(password, account.getPassword())) {
            errorMessage = resourceBundle.getString("login.login_error_cannot_find");
            valid = false;
            request.setAttribute("errorMessage", errorMessage);
        }
        if (valid) {
            String gRecaptchaResponse = request.getParameter("g-recaptcha-response");
            valid = VerifyCaptcha.verify(gRecaptchaResponse);
            if (!account.isActive()) {
                errorMessage = resourceBundle.getString("login.login_error_blocked");
                request.setAttribute("errorMessage", errorMessage);
            }
            if (!valid) {
                errorMessage = resourceBundle.getString("login.login_error_captcha");
                request.setAttribute("errorMessage", errorMessage);
            } else {
                Role userRole = Role.getRole(account);
                session.setAttribute("loguser", account);
                session.setAttribute("role", userRole);
                if (userRole == Role.ADMIN)
                    response.sendRedirect("/admin");

                if (userRole == Role.STUDENT) {
                    session.setAttribute("info", StudentDAO.getStudentByAccountId(account));
                    response.sendRedirect("/");
                }
                if (userRole == Role.TEACHER) {
                    session.setAttribute("info", TeacherDAO.getTeacherByAccountId(account));
                    response.sendRedirect("/");
                }
                return;
            }
        }
        RequestDispatcher disp = request.getRequestDispatcher("login.jsp");
        disp.forward(request, response);
    }

    /**
     * Redirects from authentication page to user's or
     * admin's page if user is logged in
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws IOException, ServletException {
        Account account = (Account) request.getSession().getAttribute("loguser");
        if (account != null) {
            Role role = Role.getRole(account);
            if (role == Role.ADMIN) {
                response.sendRedirect("/admin");
            } else {
                response.sendRedirect("/profile");
            }
        } else {
            RequestDispatcher disp = request.getRequestDispatcher("login.jsp");
            disp.forward(request, response);
        }
    }

}

