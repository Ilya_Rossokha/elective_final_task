package epam.com.controller;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/logout")
public class LogoutServlet extends javax.servlet.http.HttpServlet {
    private static final Logger LOG = Logger.getLogger(LogoutServlet.class);

    /**
     * Log out user
     * Invalidate account session
     * redirect to main page
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(javax.servlet.http.HttpServletRequest request,
                          javax.servlet.http.HttpServletResponse response)
            throws ServletException, IOException {
        LOG.trace("START TRACING LOGOUT SERVLET");

        request.getRequestDispatcher("index.jsp").include(request, response);
        HttpSession session=request.getSession();
        session.invalidate();
        response.sendRedirect("/");

    }

}

