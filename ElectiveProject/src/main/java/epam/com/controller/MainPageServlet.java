package epam.com.controller;

import epam.com.DAO.CourseDAO;
import epam.com.DAO.TeacherDAO;
import epam.com.DAO.ThemeDAO;

import epam.com.model.Course;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import javax.servlet.http.HttpSession;
import java.io.IOException;

import java.util.*;



public class MainPageServlet extends javax.servlet.http.HttpServlet {

    private static final Logger LOG = Logger.getLogger(MainPageServlet.class);

    /**
     * Display 1 page of available courses for user
     * teacher and themes in dropdown menu
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(javax.servlet.http.HttpServletRequest request,
                         javax.servlet.http.HttpServletResponse response)
            throws IOException, ServletException {
        LOG.trace("START TRACING MAIN PAGE SERVLET");

        HttpSession session = request.getSession();
        session.setAttribute("sort", null);

        List <Course> list_courses = CourseDAO.getCourses(1);
        request.setAttribute("page", 1);

        request.setAttribute("list_theme", ThemeDAO.getAllThemes());
        request.setAttribute("list_teacher", TeacherDAO.getAllTeacher());

        request.setAttribute("list_courses", list_courses);
        RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
        dispatcher.forward(request, response);
    }

}
