package epam.com.controller;

import epam.com.DAO.CourseDAO;
import epam.com.Util.Sorter;
import epam.com.model.Course;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/listcourses")
public class PaginationServlet extends javax.servlet.http.HttpServlet {
    private static final Logger LOG = Logger.getLogger(PaginationServlet.class);


    /**
     * Pagination for courses
     * calculate quantity of pages, quantity of courses
     * display courses depends of page
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */

    protected void doGet(javax.servlet.http.HttpServletRequest request,
                         javax.servlet.http.HttpServletResponse response)
            throws IOException, ServletException {
        LOG.trace("TRACING PAGINATION SERVLET");
        HttpSession session = request.getSession();


        String paramPage = request.getParameter("page");

        int page = Integer.parseInt(paramPage);

        List<Course> list_courses = CourseDAO.getCourses(page);


        int coursesNumber = CourseDAO.getCoursesNumber();
        int maxPage = (int) Math.ceil((double) coursesNumber / 10);


        if (session.getAttribute("sort") != null) {
            List<Course> courseList = (List<Course>) session.getAttribute("sort");
            list_courses = new Sorter().getCourseByPage(page, courseList);
        }

        request.setAttribute("page", page);
        request.setAttribute("maxPage", maxPage);
        request.setAttribute("list_courses", list_courses);
        RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
        dispatcher.forward(request, response);
    }
}

