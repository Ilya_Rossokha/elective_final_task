package epam.com.controller;


import epam.com.DAO.CourseDAO;

import epam.com.Util.Sorter;
import epam.com.model.Account;
import epam.com.model.Course;
import epam.com.model.Role;

import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/profile")
public class ProfileServlet extends javax.servlet.http.HttpServlet {
    private static final Logger LOG = Logger.getLogger(ProfileServlet.class);


    /**
     * Display information of user
     * user's courses
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(javax.servlet.http.HttpServletRequest request,
                         javax.servlet.http.HttpServletResponse response)
            throws ServletException, IOException {
        LOG.trace("TRACING PROFILE SERVLET");

        Sorter sorter = new Sorter();
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("loguser");
        List<Course> list_user_courses = new ArrayList<>();
        Role role = Role.getRole(account);
        if (role == Role.STUDENT) {
            list_user_courses = CourseDAO.getStudentCourses(account);
        }
        if (role == Role.TEACHER) {
            list_user_courses = CourseDAO.getTeacherCourses(account);
        }

        List<Course> list_ongoing_courses = sorter.getProgressOfCourses("ONGOING", list_user_courses);
        if (list_ongoing_courses.size() > 0) {
            request.setAttribute("list_ongoing_courses", list_ongoing_courses);
        }
        List<Course> list_soon_courses = sorter.getProgressOfCourses("SOON", list_user_courses);
        if (list_soon_courses.size() > 0) {
            request.setAttribute("list_soon_courses", list_soon_courses);
        }
        List<Course> list_finished_courses = sorter.getProgressOfCourses("FINISHED", list_user_courses);
        if (list_finished_courses.size() > 0) {
            request.setAttribute("list_finished_courses", list_finished_courses);
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher("/profile.jsp");
        dispatcher.forward(request, response);
    }

}

