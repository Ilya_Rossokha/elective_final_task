package epam.com.controller;

import epam.com.DAO.AccountDAO;
import epam.com.DAO.StudentDAO;
import epam.com.Util.MailSender;
import epam.com.Util.Regex;
import epam.com.model.Account;
import epam.com.model.Role;
import epam.com.model.Student;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;
import java.util.Random;
import java.util.ResourceBundle;

@WebServlet("/register")
public class RegisterServlet extends javax.servlet.http.HttpServlet {

    private static final Logger LOG = Logger.getLogger(RegisterServlet.class);


    /**
     * Create new account
     * redirect to login page
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        LOG.trace("TRACING REGISTER SERVLET");
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String firstname = request.getParameter("firstname");
        String surname = request.getParameter("surname");
        String email = request.getParameter("email");


        Locale locale = (Locale) request.getSession().getAttribute("locale");
        ResourceBundle resourceBundle = ResourceBundle.getBundle("local", locale);

        String errorMessage;
        if(AccountDAO.isEmailExist(email)==1){
            errorMessage = resourceBundle.getString("registration.registration_form_error_already_registered_email");
            request.setAttribute("errorMessage", errorMessage);
        }
        else if(AccountDAO.isLoginExist(login)==1){
            errorMessage = resourceBundle.getString("registration.registration_form_error_already_registered_login");
            request.setAttribute("errorMessage", errorMessage);
        }
        else if(new Regex().isLoginValid(login)){
            errorMessage = resourceBundle.getString("registration.registration_form_error_incorrect_login");
            request.setAttribute("errorMessage", errorMessage);
        }
        else if(new Regex().isEmail(email)){
            errorMessage = resourceBundle.getString("registration.registration_form_error_incorrect_email");
            request.setAttribute("errorMessage", errorMessage);
        }
        else if(new Regex().isName(firstname) && new Regex().isName(surname)){
            errorMessage = resourceBundle.getString("registration.registration_form_error_incorrect_name");
            request.setAttribute("errorMessage", errorMessage);
        }
        else {
            Account account = AccountDAO.InsertAccount(new Account(login, password, email, 0, true));
            Student student = new Student(account.getId(), firstname, surname);
            StudentDAO.insertStudent(student);
            response.sendRedirect("/login");
            return;
        }
        RequestDispatcher disp = request.getRequestDispatcher("register.jsp");
        disp.forward(request, response);
    }


    /**
     * Redirects from authentication page to user's or
     * admin's page if user is logged in
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws IOException, ServletException {
        Account account = (Account) request.getSession().getAttribute("loguser");
        if (account != null) {
            Role role = Role.getRole(account);
            if (role==Role.ADMIN) {
                response.sendRedirect("/admin");
            } else {
                response.sendRedirect("/profile");
            }
        } else {
            RequestDispatcher disp = request.getRequestDispatcher("register.jsp");
            disp.forward(request, response);
        }
    }


}

