package epam.com.controller;

import epam.com.DAO.CourseDAO;
import epam.com.DAO.TeacherDAO;
import epam.com.DAO.ThemeDAO;
import epam.com.model.Course;
import epam.com.Util.Sorter;
import epam.com.model.Teacher;
import epam.com.model.Theme;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@WebServlet("/sort")
public class SortServlet extends javax.servlet.http.HttpServlet {
    private static final Logger LOG = Logger.getLogger(SortServlet.class);

    /**
     * sort courses by A-Z, DURATION, QUANTITY, TEACHER, THEME
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(javax.servlet.http.HttpServletRequest request,
                          javax.servlet.http.HttpServletResponse response) throws ServletException, IOException {
        LOG.trace("TRACING SORT SERVLET");


        HttpSession session = request.getSession();

        String button1 = request.getParameter("button1");
        String button2 = request.getParameter("button2");
        List<Course> listCourse = CourseDAO.getCourses();
        List<Teacher> teacherList = TeacherDAO.getAllTeacher();
        List <Theme> themeList = ThemeDAO.getAllThemes();
        request.setAttribute("list_theme", themeList);
        request.setAttribute("list_teacher", teacherList);

        Sorter sorter = new Sorter();

        List <Course> courseList = CourseDAO.getCourses();


        String button = request.getParameter("sortAz");
        if(button!=null){

            courseList.sort(Comparator.comparing(Course::getDescription));

            if(session.getAttribute("sort")!=null){
                Collections.reverse(courseList);
                System.out.println(courseList);
            }
                session.setAttribute("sort", courseList);
            courseList = sorter.getCourseByPage(1,courseList);
            System.out.println(courseList);
            request.setAttribute("page",1);
            request.setAttribute("list_courses",courseList);
        }

        String sortQuantity = request.getParameter("sortQuantity");
        if(sortQuantity!=null){
            courseList.sort(Comparator.comparing(Course::getCount));

            if(session.getAttribute("sort")!=null){
                Collections.reverse(courseList);
                System.out.println(courseList);
            }
            session.setAttribute("sort", courseList);
            courseList = sorter.getCourseByPage(1,courseList);
            System.out.println(courseList);
            request.setAttribute("page",1);
            request.setAttribute("list_courses",courseList);
        }
        String sortDuration = request.getParameter("sortDuration");
        if(sortDuration!=null){
            courseList.sort(Comparator.comparing(Course::getNumDuration));

            if(session.getAttribute("sort")!=null){
                Collections.reverse(courseList);
                System.out.println(courseList);
            }
            session.setAttribute("sort", courseList);
            courseList = sorter.getCourseByPage(1,courseList);
            System.out.println(courseList);
            request.setAttribute("page",1);
            request.setAttribute("list_courses",courseList);
        }


        for (Teacher teacher : teacherList) {
            if (teacher.getFullname().equals(button1)) {
                System.out.println(sorter.getCoursesByTeacher(button1, listCourse));
                request.setAttribute("list_courses", sorter.getCoursesByTeacher(button1, listCourse));
            }
        }

        for (Theme theme : themeList) {
            if (theme.getName().equals(button2)) {
                System.out.println(sorter.getCoursesByTeacher(button2, listCourse));
                request.setAttribute("list_courses", sorter.getCoursesByTheme(button2, listCourse));
            }
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
        dispatcher.forward(request, response);
    }


    protected void doGet(javax.servlet.http.HttpServletRequest request,
                         javax.servlet.http.HttpServletResponse response) throws ServletException, IOException {


        RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
        dispatcher.forward(request, response);
    }


}

