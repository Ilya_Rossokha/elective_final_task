package epam.com.controller;

import epam.com.DAO.CourseDAO;
import epam.com.DAO.TeacherDAO;
import epam.com.DAO.ThemeDAO;
import epam.com.model.Course;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Date;

@WebServlet("/update")
public class UpdateCourseServlet extends javax.servlet.http.HttpServlet {
    private static final Logger LOG = Logger.getLogger(UpdateCourseServlet.class);


    /**
     * Edit existing course by admin
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(javax.servlet.http.HttpServletRequest request,
                          javax.servlet.http.HttpServletResponse response) throws ServletException, IOException {
        LOG.trace("TRACING UPDATE COURSE SERVLET");

        HttpSession session = request.getSession();

        Course course = (Course) session.getAttribute("crs");
        course.setDescription(request.getParameter("description"));
        course.setTeacherName(request.getParameter("teacher"));
        course.setThemeName(request.getParameter("theme"));
        course.setStart_date(Date.valueOf(request.getParameter("start_date")));
        course.setEnd_date(Date.valueOf(request.getParameter("end_date")));
        System.out.println("course + " + course);
        CourseDAO.updateCourseById(course);
        response.sendRedirect("/admin");
    }

    /**
     * Display selected course for edit
     * display list of teachers,
     * list of themes in select option
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(javax.servlet.http.HttpServletRequest request,
                         javax.servlet.http.HttpServletResponse response) throws ServletException, IOException {
        String courseId = request.getParameter("edit");
        HttpSession session = request.getSession();
        System.out.println("hey button");

        Course course = CourseDAO.getCourseById(Integer.parseInt(String.valueOf(request.getParameter("edit"))));
        System.out.println(CourseDAO.getCourseById(course.getId()));
        request.setAttribute("list_theme", ThemeDAO.getAllThemes());
        request.setAttribute("list_teacher", TeacherDAO.getAllTeacher());
        session.setAttribute("updatecourseid", courseId);
        session.setAttribute("crs", course);
        request.setAttribute("course", course); // Will be available as ${product} in JSP
        request.getRequestDispatcher("/add.jsp").forward(request, response);
    }

}

