package epam.com.controller;

import epam.com.DAO.AccountDAO;
import epam.com.model.Account;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/edit")
public class UpdateUserServlet extends javax.servlet.http.HttpServlet {
    private static final Logger LOG = Logger.getLogger(UpdateUserServlet.class);

    /**
     * Display user edit page
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(javax.servlet.http.HttpServletRequest request,
                         javax.servlet.http.HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("edituser.jsp");
        dispatcher.forward(request, response);
    }

    /**
     * Edit password for current user
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(javax.servlet.http.HttpServletRequest request,
                         javax.servlet.http.HttpServletResponse response)
            throws ServletException, IOException {
        LOG.trace("TRACING UPDATE USER SERVLET");
        HttpSession session = request.getSession();
        String errorMessage;

        // TODO error messages

        String oldPassword = request.getParameter("pass");
        String newPassword = request.getParameter("newpass");
        String passwordConfirm = request.getParameter("newpassconf");

        Account account = (Account) session.getAttribute("loguser");
        if(account!=null){
            if(!oldPassword.equals(account.getPassword())){
                errorMessage = "Invalid password";
                request.setAttribute("errorMessage", errorMessage);
            }
            else{
                if(newPassword.equals(passwordConfirm)){
                    account.setPassword(passwordConfirm);
                    AccountDAO.updatePassword(account);
                    response.sendRedirect("/profile");
                }
                return;
            }
        }
        RequestDispatcher disp = request.getRequestDispatcher("edituser.jsp");
        disp.forward(request, response);
    }

}
