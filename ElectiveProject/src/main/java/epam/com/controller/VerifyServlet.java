package epam.com.controller;

import epam.com.Util.MailSender;
import epam.com.model.Account;
import org.apache.commons.lang3.RandomStringUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/verify")
public class VerifyServlet extends javax.servlet.http.HttpServlet{

    protected void doPost(javax.servlet.http.HttpServletRequest request,
                          javax.servlet.http.HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();

        String errorMessage;
        String authcode= (String) session.getAttribute("authcode");

        String code = request.getParameter("authcode");

        if(code.equals(authcode)){
            Account account =  (Account)session.getAttribute("loguser");
            account.setVerified(true);
            response.sendRedirect("/");
        }else{
            errorMessage = "FAIL!";
            request.setAttribute("errorMessage", errorMessage);
            RequestDispatcher disp = request.getRequestDispatcher("verify.jsp");
            disp.forward(request, response);
        }
    }
    protected void doGet(javax.servlet.http.HttpServletRequest request,
                          javax.servlet.http.HttpServletResponse response) throws ServletException, IOException {

        HttpSession session  = request.getSession();
        Account account =  (Account)session.getAttribute("loguser");
        if(account.isVerified()){
            response.sendRedirect("/");
        }
        else {
            String code = RandomStringUtils.random(5, "1234567890");
            MailSender.sendMail(account, code);
            session.setAttribute("authcode", code);
            RequestDispatcher disp = request.getRequestDispatcher("verify.jsp");
            disp.forward(request, response);
        }
    }
}
