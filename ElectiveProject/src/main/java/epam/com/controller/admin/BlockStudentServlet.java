package epam.com.controller.admin;

import epam.com.DAO.AccountDAO;
import epam.com.DAO.CourseDAO;
import epam.com.DAO.StudentDAO;
import epam.com.Util.Sorter;
import epam.com.model.Course;
import epam.com.model.Student;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/students")
public class BlockStudentServlet extends javax.servlet.http.HttpServlet {
    private static final Logger LOG = Logger.getLogger(BlockStudentServlet.class);

    /**
     * Block/unblock student by admin
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(javax.servlet.http.HttpServletRequest request,
                          javax.servlet.http.HttpServletResponse response) throws ServletException, IOException {
        LOG.trace("START TRACING BLOCK STUDENT SERVLET");
        HttpSession session = request.getSession();
        if (request.getParameter("block") != null) {
            AccountDAO.updateActive(Integer.parseInt(String.valueOf(request.getParameter("block"))),0);
        }
        if (request.getParameter("unblock") != null) {
            AccountDAO.updateActive(Integer.parseInt(String.valueOf(request.getParameter("unblock"))),1);
        }


        response.sendRedirect(request.getRequestURI() + "?page=" + session.getAttribute("page"));

    }

    /**
     * Display list of students with pagination
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(javax.servlet.http.HttpServletRequest request,
                         javax.servlet.http.HttpServletResponse response) throws ServletException, IOException {
        String paramPage = request.getParameter("page");
        int page = Integer.parseInt(paramPage);

        List <Student> studentList = StudentDAO.getAllStudents(page);

        int studensNumber = StudentDAO.getStudensNumber();
        int maxPage = (int) Math.ceil((double) studensNumber / 10);


        request.setAttribute("page", page);
        HttpSession session = request.getSession();
        session.setAttribute("page",page);
        request.setAttribute("maxPage", maxPage);
        request.setAttribute("list_students", studentList);
        RequestDispatcher dispatcher = request.getRequestDispatcher("students.jsp");
        dispatcher.forward(request, response);

    }
}
