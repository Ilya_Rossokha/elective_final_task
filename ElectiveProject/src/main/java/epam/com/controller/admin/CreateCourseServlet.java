package epam.com.controller.admin;

import epam.com.DAO.CourseDAO;
import epam.com.DAO.TeacherDAO;
import epam.com.DAO.ThemeDAO;
import epam.com.model.Course;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.sql.Date;

@WebServlet("/create")
public class CreateCourseServlet extends javax.servlet.http.HttpServlet {

    private static final Logger LOG = Logger.getLogger(CreateCourseServlet.class);

    /**
     * New course by admin
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(javax.servlet.http.HttpServletRequest request,
                          javax.servlet.http.HttpServletResponse response) throws IOException, ServletException {
        LOG.trace("START TRACING CREATE COURSE SERVLET");

        String description = request.getParameter("description");
        Date start_date = Date.valueOf(request.getParameter("start_date"));
        Date end_date = Date.valueOf(request.getParameter("end_date"));
        String theme_name = request.getParameter("theme");
        String teacher_name = request.getParameter("teacher");
        Course course = new Course();
        course.setDescription(description);
        course.setStart_date(start_date);
        course.setEnd_date(end_date);
        course.setThemeName(String.valueOf(ThemeDAO.getThemeByName(theme_name).getId()));
        course.setTeacherName(String.valueOf(TeacherDAO.getTeacherByFullName(teacher_name).getId()));
        CourseDAO.InsertCourse(course);
        response.sendRedirect("/admin");
    }

    protected void doGet(javax.servlet.http.HttpServletRequest request,
                         javax.servlet.http.HttpServletResponse response)
            throws IOException, ServletException {
        request.setAttribute("list_theme", ThemeDAO.getAllThemes());
        request.setAttribute("list_teacher", TeacherDAO.getAllTeacher());
        System.out.println(ThemeDAO.getAllThemes());
        System.out.println(TeacherDAO.getAllTeacher());
        RequestDispatcher dispatcher = request.getRequestDispatcher("add.jsp");
        dispatcher.forward(request, response);
    }


}
