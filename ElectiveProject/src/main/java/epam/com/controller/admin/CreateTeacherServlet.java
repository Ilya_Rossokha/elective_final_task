package epam.com.controller.admin;

import epam.com.DAO.AccountDAO;
import epam.com.DAO.TeacherDAO;
import epam.com.Util.MailSender;
import epam.com.Util.PasswordService;
import epam.com.Util.Regex;
import epam.com.model.Account;
import epam.com.model.Teacher;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;

@WebServlet("/teacher")
public class CreateTeacherServlet extends javax.servlet.http.HttpServlet {
    private static final Logger LOG = Logger.getLogger(CreateTeacherServlet.class);

    /**
     * New teacher by admin
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(javax.servlet.http.HttpServletRequest request,
                          javax.servlet.http.HttpServletResponse response) throws ServletException, IOException {
        LOG.trace("START TRACING CREATE TEACHER SERVLET");
        String login = request.getParameter("login");
        String email = request.getParameter("email");
        String firstname = request.getParameter("firstname");
        String surname = request.getParameter("surname");
        String errorMessage;


        if(new Regex().isEmail(email)){
            errorMessage = "Incorrect email";
            request.setAttribute("errorMessage", errorMessage);
        }
        else {
            String password = PasswordService.hashPassword(PasswordService.generatePassword());
            Account account = new Account(login, password, email, 1, true);
            account.setVerified(true);
            AccountDAO.InsertAccount(account);
            Teacher teacher = new Teacher(account.getId(), firstname, surname);
            TeacherDAO.insertTeacher(teacher);
            MailSender.sendMail(account,"No code");
            response.sendRedirect("/");
            return;
        }
        RequestDispatcher disp = request.getRequestDispatcher("teacher.jsp");
        disp.forward(request, response);
    }
    protected void doGet(javax.servlet.http.HttpServletRequest request,
                         javax.servlet.http.HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/teacher.jsp");
        dispatcher.forward(request, response);
    }
}
