package epam.com.controller.admin;

import epam.com.DAO.ThemeDAO;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;

@WebServlet("/theme")
public class CreateThemeServlet extends javax.servlet.http.HttpServlet {
    private static final Logger LOG = Logger.getLogger(CreateThemeServlet.class);

    /**
     * New theme by admin
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(javax.servlet.http.HttpServletRequest request,
                          javax.servlet.http.HttpServletResponse response) throws ServletException, IOException {
        LOG.trace("START TRACING CREATE THEME SERVLET");
        String theme_name = request.getParameter("theme_name");
        ThemeDAO.insertTheme(theme_name);
        response.sendRedirect("/create");
    }
    protected void doGet(javax.servlet.http.HttpServletRequest request,
                          javax.servlet.http.HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("/theme.jsp");
        dispatcher.forward(request, response);
    }
}
