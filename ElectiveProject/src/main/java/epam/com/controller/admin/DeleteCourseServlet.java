package epam.com.controller.admin;

import epam.com.DAO.*;
import epam.com.model.Course;
import epam.com.model.Student;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.util.List;

@WebServlet("/admin")
public class DeleteCourseServlet extends javax.servlet.http.HttpServlet {
    private static final Logger LOG = Logger.getLogger(DeleteCourseServlet.class);


    /**
     * Delete course by admin
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(javax.servlet.http.HttpServletRequest request,
                          javax.servlet.http.HttpServletResponse response) throws ServletException, IOException {
        LOG.trace("START TRACING DELETE/BLOCK-UNBLOCK COURSE/STUDENT SERVLET");

        if (request.getParameter("delete") != null) {
            for (Course course : CourseDAO.getCourses()) {
                if (Integer.parseInt(String.valueOf(request.getParameter("delete"))) == course.getId()) {
                    CourseDAO.deleteCourse(course);
                }
            }
        }

        request.setAttribute("list_courses",CourseDAO.getCourses());

        response.sendRedirect("/admin");

}

    protected void doGet(javax.servlet.http.HttpServletRequest request,
                         javax.servlet.http.HttpServletResponse response) throws ServletException, IOException {
        List<Course> list_courses = CourseDAO.getCourses();
        request.setAttribute("list_courses", list_courses);
        RequestDispatcher dispatcher = request.getRequestDispatcher("admin.jsp");
        dispatcher.forward(request, response);
    }
}
