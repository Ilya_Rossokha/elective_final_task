package epam.com.filter;

import epam.com.model.Role;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

@WebFilter(filterName = "AdminFilter")
public class AdminFilter implements Filter {
    List<String> adminPages;
    List <String> authPages;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        adminPages = new ArrayList<>();
        adminPages.add("/admin");
        adminPages.add("/theme");
        adminPages.add("/teacher");
        adminPages.add("/create");
        authPages = new ArrayList<>();
        authPages.add("/login");
        authPages.add("/register");
    }
    /**
     * Filter request to admin's pages
     * Denying permission if not admin
     * user's courses
     * @param servletRequest
     * @param servletResponse
     * @param filterChain
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);
        String URI = request.getRequestURI();


        boolean isAdmin = (session != null && session.getAttribute("role")== Role.ADMIN);
        boolean isResource = request.getRequestURI().startsWith(request.getContextPath() + "/");



        String errorMessage = null;


        if(!isAdmin){
            for (String s:adminPages){
                if(URI.contains(s)){
                    System.out.println("adminPAGE! ");
                    errorMessage = "Permission denied";
                    request.setAttribute("errorMessage", errorMessage);
                    RequestDispatcher disp = request.getRequestDispatcher("error_page.jsp");
                    disp.forward(request, response);
                }
            }
        }
        if(isResource)
            filterChain.doFilter(request, response);

    }

    @Override
    public void destroy() {

    }
}
