package epam.com.filter;

import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;


@WebFilter(filterName = "LanguageFilter")
public class LanguageFilter implements Filter {

    public static final Logger LOG = Logger.getLogger(LanguageFilter.class);


    public void destroy() {
    }
    public void init(FilterConfig config) throws ServletException {

    }
    /**
     * Checks if language was not changed.
     * @param servletRequest
     * @param servletResponse
     * @param filterChain
     * @throws ServletException
     * @throws IOException
     */
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession();


        Locale locale = (Locale) session.getAttribute("locale");
        String loc = request.getParameter("loc");


        if (locale != null) {
            if (loc != null) {
                Locale locStartPage = new Locale(loc);
                session.setAttribute("locale", locStartPage);
                response.sendRedirect("/");
            } else {
                filterChain.doFilter(servletRequest, servletResponse);
            }
        } else {
            session.setAttribute("locale", new Locale("en"));
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }


}
