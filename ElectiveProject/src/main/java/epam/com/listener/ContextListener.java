package epam.com.listener;


import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener()
public class ContextListener implements ServletContextListener{

    private static final Logger LOG = Logger.getLogger(ContextListener.class);


    public ContextListener() {
    }

    public void contextInitialized(ServletContextEvent event) {
        LOG.trace("Servlet context initialization starts");

        ServletContext servletContext = event.getServletContext();
        initLog4J(servletContext);

        LOG.trace("Servlet context initialization finished");
    }

    public void contextDestroyed(ServletContextEvent sce) {
        LOG.trace("Servlet context destruction starts");
        LOG.trace("Servlet context destruction finished");
    }

    /**
     * Initialize log4j at startup web application
     * @param servletContext Context of servlet
     */
    private void initLog4J(ServletContext servletContext) {
        LOG.trace("Log4J initialization started");
        try {
            PropertyConfigurator.configure(servletContext.getRealPath("WEB-INF/log4j.properties"));
            LOG.debug("Log4j has been initialized");
        } catch (Exception ex) {
            LOG.trace("Cannot configure Log4j");
            ex.printStackTrace();
        }
        LOG.trace("Log4J initialization finished");
    }

}
