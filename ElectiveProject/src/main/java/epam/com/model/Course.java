package epam.com.model;

import java.sql.Date;
import java.util.Comparator;
import java.util.Objects;

public class Course implements Comparable<Course> {
    private int id;
    private String description;
    private String themeName;
    private String teacherName;
    private String progress;
    private String duration;
    private int numDuration;

    private int count;
    private Date start_date;
    private Date end_date;

    private int grade;
    public Course() {
    }
    public Course(int id) {
        this.id = id;
    }


    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }


    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }


    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(Date start, Date end) {
        long durationL = (end.getTime() - start.getTime()) / (1000 * 60 * 60 * 24)
                % 365;
        int days = (int) (durationL % 30);
        int month = (int) (durationL / 30);
        if (month == 0) {
            this.duration = days + " days";
        } else {
            duration = month + " month " + days + " days";
        }
    }

    public int getNumDuration() {
        return numDuration;
    }

    public void setNumDuration(Date start, Date end) {
        long diff = end.getTime() - start.getTime();
        long diffDays = diff / (24 * 60 * 60 * 1000) + 1;
        numDuration = (int) diffDays;
    }

    public String getThemeName() {
        return themeName;
    }

    public void setThemeName(String themeName) {
        this.themeName = themeName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Course course = (Course) o;
        return Objects.equals(description, course.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description);
    }


    public String progress(Date start, Date end) {
        long millis = System.currentTimeMillis();
        java.sql.Date date = new java.sql.Date(millis);
        if (end.compareTo(date) >= 0 && start.compareTo(date) <= 0) {
            return Progress.PROGRESS_ONGOING.getProgress();
        } else if (start.compareTo(date) > 0) {
            return Progress.PROGRESS_SOON.getProgress();
        }
        return Progress.PROGRESS_FINISHED.getProgress();
    }
    @Override
    public int compareTo(Course o) {
        int result = description.compareTo(o.description);
        if (result != 0) {
            return result;
        }
        result = Integer.compare(getCount(), o.getCount());
        if (result != 0) {
            return result;
        }
        result = Integer.compare(getNumDuration(), o.getNumDuration());
        if (result != 0) {
            return result;
        }
        return 0;
    }



    @Override
    public String toString() {
        return numDuration + id + " " + progress + " " + description + "  " + themeName + " " + teacherName + " duration = " + duration + start_date;
    }


}
