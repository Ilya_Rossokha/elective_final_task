package epam.com.model;

public enum Progress {
    PROGRESS_SOON("SOON"),
    PROGRESS_ONGOING("ONGOING"),
    PROGRESS_FINISHED("FINISHED");

    private final String value;

    private Progress(String value){
        this.value = value;
    }

    public String getProgress(){
        return value;
    }
}
