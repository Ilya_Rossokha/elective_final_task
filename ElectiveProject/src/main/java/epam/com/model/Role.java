package epam.com.model;

public enum Role {
    STUDENT, TEACHER, ADMIN;

    public static Role getRole(Account account) {
        int roleId = account.getRole();
        return Role.values()[roleId];
    }

    public String getName() {
        return name().toLowerCase();
    }

}
