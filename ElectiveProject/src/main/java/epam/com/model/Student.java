package epam.com.model;

public class Student {
    private int id;
    private int account_id;
    private String firstname;
    private String surname;
    private int grade;
    private int isBlocked;

    public Student(){

    }

    public Student(int account_id,String firstname, String surname) {
        this.account_id = account_id;
        this.firstname = firstname;
        this.surname = surname;
    }

    public Integer getIsBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(Integer isBlocked) {
        this.isBlocked = isBlocked;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAccount_id() {
        return account_id;
    }

    public void setAccount_id(int account_id) {
        this.account_id = account_id;
    }

    @Override
    public String toString() {
        return isBlocked + " " + id + " acc_id " + account_id + " name : " + firstname + " surname: " + surname + " grade = " + grade;
    }
}
