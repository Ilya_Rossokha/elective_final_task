package epam.com.model;

public class Teacher {
    private int id;
    private int account_id;
    private String firstname;
    private String surname;

    private String fullname;
    public Teacher(){

    }

    public Teacher(int account_id,String firstname, String surname) {
        this.account_id = account_id;
        this.firstname = firstname;
        this.surname = surname;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAccount_id() {
        return account_id;
    }

    public void setAccount_id(int account_id) {
        this.account_id = account_id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return firstname + " " + surname + " " + account_id;
    }
}
