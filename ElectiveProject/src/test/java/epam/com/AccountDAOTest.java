package epam.com;

import epam.com.DAO.AccountDAO;
import epam.com.DAO.StudentDAO;
import epam.com.model.Account;
import epam.com.model.Student;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AccountDAOTest {
    Account account;
    @Before
    public void setUp() throws Exception {
        account = new Account();
        account.setLogin("testLogin");
        account.setPassword("123321");
        account.setEmail("email@mail.com");
        account.setRole(0);
        account.setActive(true);
        AccountDAO.InsertAccount(account);

    }

    @After
    public void tearDown(){
        AccountDAO.deleteAccount(account);
    }

    @Test
    public void insertAccount() throws Exception {
        AccountDAO.deleteAccount(account);
        AccountDAO.InsertAccount(account);
        assertEquals(account.getLogin(), AccountDAO.getAccount("testLogin").getLogin());
    }

    @Test
    public void deleteAccount() throws Exception {
        AccountDAO.deleteAccount(account);
        assertNull(AccountDAO.getAccount("testLogin").getLogin());
    }

    @Test
    public void updateActive() throws Exception {
        AccountDAO.updateActive(account.getId(),1);
        assertTrue(AccountDAO.getAccount("testLogin").isActive());
    }

    @Test
    public void updatePassword() throws Exception {
        account.setPassword("new");
        AccountDAO.updatePassword(account);
        assertEquals("new",AccountDAO.getAccount("testLogin").getPassword());
    }


}
