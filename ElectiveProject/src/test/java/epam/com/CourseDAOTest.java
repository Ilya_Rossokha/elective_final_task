package epam.com;

import com.mysql.cj.jdbc.MysqlConnectionPoolDataSource;
import epam.com.DAO.CourseDAO;
import epam.com.DAO.ThemeDAO;
import epam.com.model.Course;
import org.apache.log4j.PropertyConfigurator;
import org.apache.naming.java.javaURLContextFactory;
import org.junit.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import java.sql.Date;
import java.util.List;

import static org.junit.Assert.*;

public class CourseDAOTest {
    private Course course;

    @Before
    public void setUp() throws Exception {
        course = new Course();
        course.setDescription("javascript functions");
        course.setTeacherName("1");
        course.setThemeName("41");
        course.setStart_date(Date.valueOf("2021-04-04"));
        course.setEnd_date(Date.valueOf("2021-06-06"));

        CourseDAO.InsertCourse(course);
    }

    @After
    public void tearDown(){
        CourseDAO.deleteCourse(course);
    }

    @Test
    public void insertCourse() throws Exception {
        CourseDAO.deleteCourse(course);
        int expected = CourseDAO.getCourses().size() + 1;
        CourseDAO.InsertCourse(course);
        assertEquals(expected, CourseDAO.getCourses().size());
    }

    @Test
    public void deleteCourse() throws Exception {
        int expected = CourseDAO.getCourses().size();
        CourseDAO.InsertCourse(course);
        CourseDAO.deleteCourse(course);
        assertEquals(expected, CourseDAO.getCourses().size());
    }

    @Test
    public void updateCourse() throws Exception {
        String expected = course.getDescription();
        course.setDescription("dsadas");
        course.setTeacherName("Antonio Teacher");
        course.setThemeName("java");
        CourseDAO.updateCourseById(course);
        assertNotEquals(expected, course.getTeacherName());
    }

    @Test
    public void findAllCourses() throws Exception {
        List<Course> courseList = CourseDAO.getCourses();
        assertNotNull(courseList.get(0));
    }
}
