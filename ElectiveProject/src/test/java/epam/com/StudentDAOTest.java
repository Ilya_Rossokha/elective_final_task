package epam.com;

import com.mysql.cj.jdbc.MysqlConnectionPoolDataSource;
import epam.com.DAO.AccountDAO;
import epam.com.DAO.CourseDAO;
import epam.com.DAO.StudentDAO;
import epam.com.DAO.ThemeDAO;
import epam.com.model.Account;
import epam.com.model.Student;
import epam.com.model.Theme;
import org.apache.log4j.PropertyConfigurator;
import org.apache.naming.java.javaURLContextFactory;
import org.junit.*;

import javax.naming.Context;
import javax.naming.InitialContext;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class StudentDAOTest {
    private Account account;


    @Before
    public void setUp() throws Exception {
        account = new Account();
        account.setLogin("testLogin");
        account.setPassword("123321");
        account.setEmail("email@mail.com");
        account.setRole(0);
        account.setActive(true);
        AccountDAO.InsertAccount(account);

    }

    @After
    public void tearDown(){
        AccountDAO.deleteAccount(account);
    }

    @Test
    public void insertStudent() throws Exception {
        AccountDAO.deleteAccount(account);
        int expected = StudentDAO.getStudensNumber()+1;
        AccountDAO.InsertAccount(account);
        Student student = new Student();
        student.setFirstname("Student");
        student.setSurname("Stud");
        student.setAccount_id(account.getId());
        StudentDAO.insertStudent(student);
        assertEquals(expected, StudentDAO.getStudensNumber());
    }

    @Test
    public void findAllStudents() throws Exception {
        List<Student> studentList = StudentDAO.getAllStudents(1);
        assertNotNull(studentList.get(0));
    }
}
