package epam.com;

import com.mysql.cj.jdbc.MysqlConnectionPoolDataSource;
import epam.com.DAO.AccountDAO;
import epam.com.DAO.TeacherDAO;
import epam.com.model.Account;
import epam.com.model.Teacher;
import org.apache.log4j.PropertyConfigurator;
import org.apache.naming.java.javaURLContextFactory;
import org.junit.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TeacherDAOTest {
    private Teacher teacher;
    private Account account;

    @Before
    public void setUp() throws Exception {
        account = new Account();
        account.setLogin("testLogin");
        account.setPassword("123321");
        account.setEmail("email@mail.com");
        account.setRole(1);
        account.setActive(true);
        AccountDAO.InsertAccount(account);

    }

    @After
    public void tearDown(){
        AccountDAO.deleteAccount(account);
    }


    @Test
    public void insertTeacher() throws Exception {
        AccountDAO.deleteAccount(account);
        int expected = TeacherDAO.getAllTeacher().size()+1;
        AccountDAO.InsertAccount(account);
        teacher = new Teacher();
        teacher.setFirstname("Teacher");
        teacher.setSurname("Teach");
        teacher.setAccount_id(account.getId());
        TeacherDAO.insertTeacher(teacher);
        assertEquals(expected, TeacherDAO.getAllTeacher().size());
    }

    @Test
    public void findAllTeachers() throws Exception {
        List<Teacher> teacherList = TeacherDAO.getAllTeacher();
        assertNotNull(teacherList.get(0));
    }

    @Test
    public void getTeacherByFullName() throws Exception {
        teacher = new Teacher();
        teacher.setFirstname("Teacher");
        teacher.setSurname("Teach");
        teacher.setAccount_id(account.getId());
        TeacherDAO.insertTeacher(teacher);
        Teacher teacherTest = TeacherDAO.getTeacherByFullName("Teacher Teach");
        assertEquals(teacher.getFirstname(), teacherTest.getFirstname());
    }

}
