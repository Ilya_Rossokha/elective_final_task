package epam.com;

import com.mysql.cj.jdbc.MysqlConnectionPoolDataSource;
import epam.com.DAO.ThemeDAO;
import epam.com.model.Theme;
import org.apache.log4j.PropertyConfigurator;
import org.apache.naming.java.javaURLContextFactory;
import org.junit.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import java.util.List;

import static org.junit.Assert.*;

public class ThemeDAOTest {
    private Theme theme;

    @Before
    public void setUp() throws Exception {
        theme = new Theme();
        theme.setName("unity");
    }

    @After
    public void tearDown(){
        ThemeDAO.deleteTheme("unity");
    }

    @Test
    public void insertTheme() throws Exception {
        int expected = ThemeDAO.getAllThemes().size() + 1;
        ThemeDAO.insertTheme(theme.getName());
        assertEquals(expected, ThemeDAO.getAllThemes().size());
    }

    @Test
    public void findAllThemes() throws Exception {
        List<Theme> themeList = ThemeDAO.getAllThemes();
        assertNotNull(themeList);
    }
}
