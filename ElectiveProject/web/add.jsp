<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<c:choose>
    <c:when test="${locale == 'ru'}">
        <fmt:setLocale value="ru"/>
    </c:when>
    <c:otherwise>
        <fmt:setLocale value="en"/>
    </c:otherwise>
</c:choose>
<fmt:setBundle basename="local"/>
<html>
<head>
    <title>Admin Course Page</title>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW"
            crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../../styles/add.css">
    <link rel='icon' type='image/x-icon' href='../../image/admin.ico'/>
</head>
<body>
<jsp:include page="header2.jspf"/>
<div class="masthead">
    <div class="signup-form">
        <div class="d-flex">
            <div class="dropdown me-1">
                <button type="button" class="btn-sm btn-success dropdown-toggle " id="dropdownMenuOffset"
                        data-bs-toggle="dropdown" aria-expanded="false" data-bs-offset="10,20">
                    <fmt:message key="admin.page_create_course_new_theme" var="newTh"/>
                    ${newTh}
                </button>
                <div class="dropdown-menu">
                    <form action="/theme" method="post" class="px-4 py-3">
                        <div class="form-group">
                            <div class="input-group">
                                <fmt:message key="admin.page_create_course_new_theme_name" var="thN"/>
                                <input type="text" size="10" class="form-control" name="theme_name" placeholder="${thN}"
                                       required="required">
                            </div>
                        </div>
                        <fmt:message key="admin.page_create_course_button_create" var="create"/>
                        <button type="submit" class="btn btn-success">${create}</button>
                    </form>

                </div>
            </div>
        </div>
        <c:if test="${course != null}">
        <form action="/update" method="post">
            </c:if>
            <c:if test="${course == null}">
                <p class="par" id="paragraph">Successfully added!</p>
                <form action="/create" id="myForm" method="post">
        </c:if>
            <fmt:message key="admin.page_create_course" var="crs"/>
        <h2>${crs}</h2>
        <hr>
        <div class="form-group">
            <div  class="input-group">
                <fmt:message key="admin.page_create_course_description" var="desc"/>
                <input type="text" size="100" class="form-control" value="<c:out value='${course.description}'/>" name="description" placeholder="${desc}" required="required">
            </div>
        </div>

        <div class="form-group">
            <div  class="input-group">
                <div class="input-group-prepend">
					<span class="input-group-text">
					<fmt:message key="admin.page_create_course_theme" var="thC"/>
					<p>	<span class="letter" aria-hidden="true">${thC}</span> </p>
					</span>
                </div>
                <select name="theme"  class="form-control">
                    <c:forEach items="${list_theme}" var="theme">
                        <c:choose>
                        <c:when test="${course.themeName == theme.name}">
                        <option selected value="${theme.name}"><c:out value="${theme.name}"/></option>
                        </c:when>
                        <c:otherwise>
                        <option value="${theme.name}"><c:out value="${theme.name}"/></option>
                        </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
            </div>
        </div>



        <div class="form-group">
            <div  class="input-group">
                <div class="input-group-prepend">
					<span class="input-group-text">
						<span class="fa fa-user" aria-hidden="true"></span>
						  <fmt:message key="admin.page_create_course_teacher" var="teachC"/>
						<p>	<span class="letter" aria-hidden="true">${teachC}</span> </p>
					</span>
                </div>
                <select name="teacher"  class="form-control" >
                    <c:forEach items="${list_teacher}" var="teacher">
                        <c:choose>
                        <c:when test="${course.teacherName == teacher.fullname}">
                        <option selected value="${teacher.fullname}"><c:out value="${teacher.fullname}"/></option>
                        </c:when>
                            <c:otherwise>
                                <option value="${teacher.fullname}"><c:out value="${teacher.fullname}"/></option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
            </div>
        </div>

        <div class="form-group">
            <div  class="input-group">
                <div class="input-group-prepend">
					<span class="input-group-text">
						  <fmt:message key="admin.page_create_course_start" var="startd"/>
						<p>	<span class="letter" aria-hidden="true">${startd}</span> </p>
					</span>
                </div>
                <input class="form-control" type="date"  placeholder="start_date" name="start_date" value="<c:out value='${course.start_date}' />" required>
            </div>
        </div>
        <div class="form-group">
            <div  class="input-group">
                <div class="input-group-prepend">
					<span class="input-group-text">
						  <fmt:message key="admin.page_create_course_end" var="endD"/>
						<p>	<span class="letter" aria-hidden="true">${endD}</span> </p>
					</span>
                </div>

                <input class="form-control" type="date" placeholder="end_date" name="end_date" value="<c:out value='${course.end_date}' />"
                       required>
            </div>
        </div>
        <div class="form-group">
            <fmt:message key="admin.page_create_course_button_create" var="cr"/>
            <button type="submit" id="mySubmit" onclick="onButtonClick()" class="btn btn-primary">${cr}</button>
        </div>
        </form>
    </div>
</div>
<script>
    const par = document.getElementById('paragraph');

    function onButtonClick() {
        par.style.display = 'block';

        setTimeout(() => {
            par.style.display = 'none';
        }, 5000)
    }

    var myForm = document.getElementById('myForm');

    myForm.addEventListener("submit", function(evt) {
        var elemSubmit = document.getElementById('mySubmit');
        elemSubmit.setAttribute("disabled", "disabled");

        // Removes disabling after 3 seconds
        window.setTimeout(function() {
            elemSubmit.removeAttribute("disabled");
        }, 5000);
    },false);
</script>
</body>
</html>
