<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<c:choose>
    <c:when test="${locale == 'ru'}">
        <fmt:setLocale value="ru"/>
    </c:when>
    <c:otherwise>
        <fmt:setLocale value="en"/>
    </c:otherwise>
</c:choose>
<fmt:setBundle basename="local"/>
<html>
<head>
    <title>Admin</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel='icon' type='image/x-icon' href='../../image/admin.ico' />
    <style>
        .main-content{
            position: relative;
           top: 70px;
        }
    </style>
</head>
<body>
<%@include file="header.jspf"%>
<div class="main-content">
<table class="table table-dark">
        <thead class="thead-light">
        <tr>
            <th scope="col">ID</th>
            <fmt:message key="admin.page_course_description" var="desc"/>
            <th scope="col">${desc}</th>
            <fmt:message key="admin.page_course_theme" var="th"/>
            <th scope="col">${th}</th>
            <fmt:message key="admin.page_course_teacher" var="teach"/>
            <th scope="col">${teach}</th>
            <fmt:message key="admin.page_course_start_date" var="std"/>
            <th scope="col">${std}</th>
            <fmt:message key="admin.page_course_end_date" var="end"/>
            <th scope="col">${end}</th>
            <fmt:message key="admin.page_course_edit" var="ed"/>
            <th scope="col">${ed}</th>
            <fmt:message key="admin.page_course_delete" var="del"/>
            <th scope="col">${del}</th>

        </tr>
        </thead>
        <c:forEach items="${list_courses}" var="course">
            <tbody>
            <tr>
                <td><c:out value="${course.id}" /></td>
                <td><c:out value="${course.description}" /></td>
                <td><c:out value="${course.themeName}" /></td>
                <td><c:out value="${course.teacherName}" /></td>
                <td><c:out value="${course.start_date}" /></td>
                <td><c:out value="${course.end_date}" /></td>
                <form action="/update" method="get">
                    <td><button class="btn btn-warning" type="submit" name="edit"  value="<c:out value="${course.id}"/>">${ed}</button></td>
                </form>
                <form action="/admin" method="post">
                    <fmt:message key="admin.page_course_delete_confirmation" var="cblck"/>
                    <td><button class="btn btn-danger" type="submit" name="delete" value="<c:out value="${course.id}"/>" onclick="return confirm('${cblck}');">${del}</button></td>
                </form>
            </tr>
            </tbody>
        </c:forEach>
    </table>
</div>
</body>
</html>
