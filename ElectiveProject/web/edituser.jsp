<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<c:choose>
    <c:when test="${locale == 'ru'}">
        <fmt:setLocale value="ru"/>
    </c:when>
    <c:otherwise>
        <fmt:setLocale value="en"/>
    </c:otherwise>
</c:choose>
<fmt:setBundle basename="local"/>
<html>
<head>
    <title>Change Password Page</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Bitter:400,700">
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <link rel='icon' type='image/x-icon' href='../../image/password.ico' />
    <style>
        .signup-form {
            width: 400px;
            margin: 0 auto;
            padding: 100px 0;
            text-align-last: center;
            text-align: center;
            background-color: white;

        }
        .form-group {
            display: flex;
            justify-content: center;
            padding: 10px;
        }
        .errormes{
            background-color:red;
            color:white;
            font-size: 25px;
            font-family: 'Nunito', sans-serif;
        }
    </style>
</head>
<body>
<div class="signup-form">
<form action="/edit" method="post">
    <c:set var="message" value="${requestScope['errorMessage']}"/>
    <c:if test="${not empty message}">
        <p class="errormes">${message}</p>
    </c:if>

    <hr>
    <div class="form-group">
        <div class="input-group">
            <div class="input-group-prepend">
					<span class="input-group-text">
						<i class="fa fa-lock"></i>
					</span>
            </div>
            <input type="password" class="form-control input-lg"  name="pass" placeholder="Current Password" required="required">
        </div>
    </div>
    <div class="form-group">
        <div class="input-group">
            <div class="input-group-prepend">
					<span class="input-group-text">
						<i class="fa fa-lock"></i>
					</span>
            </div>
            <input type="password" class="form-control input-lg"  name="newpass" placeholder="New Password" required="required">
        </div>
    </div>
    <div class="form-group">
        <div class="input-group">
            <div class="input-group-prepend">
					<span class="input-group-text">
						<i class="fa fa-lock"></i>
					</span>
            </div>
            <input type="password" class="form-control input-lg"  name="newpassconf" placeholder="Confirm Password" required="required">
        </div>
    </div>
<button type="submit" class="btn btn-primary btn-block">Update password</button>
</form>
</div>

</body>
</html>
