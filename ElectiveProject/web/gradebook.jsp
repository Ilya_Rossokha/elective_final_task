<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<c:choose>
    <c:when test="${locale == 'ru'}">
        <fmt:setLocale value="ru"/>
    </c:when>
    <c:otherwise>
        <fmt:setLocale value="en"/>
    </c:otherwise>
</c:choose>
<fmt:setBundle basename="local"/>
<html>
<head>
    <title>Grade book Page</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../styles/gradebook.css">
    <link rel='icon' type='image/x-icon' href='../../image/gradebook.ico' />
</head>
<body>
<table class="table table-striped">
    <thead class="table-bordered">
    <tr>
        <fmt:message key="admin.page_student_firstname" var="na"/>
        <th scope="col">${na}</th>
        <fmt:message key="admin.page_student_surname" var="sr"/>
        <th scope="col">${sr}</th>
        <fmt:message key="teacher.page_gradebook_grade" var="grd"/>
        <th scope="col">${grd}</th>
    </tr>
    </thead>
    <c:forEach items="${list_students}" var="student">
    <tbody class="table-bordered">
        <tr>
        <td><c:out value="${student.firstname}"/></td>
        <td><c:out value="${student.surname}"/></td>
            <td>
                <form action="/gradebook" method="post">
                <select class="form-control" name="grade">
                    <option selected value="<c:out value="${student.grade}" />"><c:out value="${student.grade}"/></option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </td>
            </tr>
        </tbody>
    </c:forEach>
</table>
<div class="center">
    <fmt:message key="teacher.page_gradebook_save" var="sv"/>
<button type="submit" class="btn btn-success btn-lg">${sv}</button>
</div>
</form>
</body>
</html>
