<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<c:choose>
    <c:when test="${locale == 'ru'}">
        <fmt:setLocale value="ru"/>
    </c:when>
    <c:otherwise>
        <fmt:setLocale value="en"/>
    </c:otherwise>
</c:choose>

<fmt:setBundle basename="local"/>

<html>
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW"
            crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Bitter:400,700">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../styles/main.css">
    <link rel='icon' type='image/x-icon' href='../../image/course.ico' />
    <fmt:message key="main.title" var="title"/>
    <title>${title}</title>
</head>
<body>
<%@include file="header.jspf"%>

<header class="masthead">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12 text-center">
                <fmt:message key="main.available_courses" var="available"/>
                <c:set var="user" value="${sessionScope['loguser']}"/>
                <c:if test="${user.verified == false && user.role == 0}">
                    <fmt:message key="main.verify_email" var="ve"/>
                    <fmt:message key="main.verify_here" var="here"/>
                <p class="verify">${ve} <a href="/verify">${here}</a></p>
                </c:if>
                <h1 class="font-weight-light">${available}</h1>
                <nav class="navbar navbar-expand-lg">
                    <div class="container">
                        <div class="collapse navbar-collapse" id="navbarResponsive">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item active">
                                    <fmt:message key="main.button.sort" var="sort"/>
                                    <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown">${sort}
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <form action="${pageContext.request.contextPath}/sort" method="post">
                                            <li>
                                                <fmt:message key="main.button.sort_az" var="az"/>
                                                <button type="button" class="btn btn-info" type="submit" name="sortAz" value="button1">${az}</button>
                                            </li>
                                            <li>
                                                <fmt:message key="main.button.sort_quantity" var="byQuantity"/>
                                                <button type="button" class="btn btn-info" type="submit" name="sortQuantity" value="button2">${byQuantity}</button>
                                            </li>
                                            <li>
                                                <fmt:message key="main.button.sort_duration" var="byDuration"/>
                                                <button type="button" class="btn btn-info" type="submit" name="sortDuration" value="button3">${byDuration}</button>
                                            </li>
                                        </form>
                                    </ul>
                                </li>
                                <div class="dropdown">
                                    <fmt:message key="main.button_sort_teacher" var="byTeacher"/>
                                    <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown">${byTeacher}
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">

                                        <c:forEach items="${list_teacher}" var="teacher">
                                        <form action="${pageContext.request.contextPath}/sort" method="post">
                                            <li>
                                                <button type="button" class="btn btn-info" type="submit" name="button1"
                                                        value="${teacher.fullname}">${teacher.fullname}</button>
                                            </li>
                                            </c:forEach>
                                        </form>
                                    </ul>
                                </div>
                                <div class="dropdown">
                                    <fmt:message key="main.button_sort_theme" var="byTheme"/>
                                    <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown">${byTheme}
                                        <span class="caret"></span></button>
                                    <ul class="dropdown-menu">

                                        <c:forEach items="${list_theme}" var="theme">

                                        <form action="${pageContext.request.contextPath}/sort" method="post">
                                            <li>
                                                <button type="button" class="btn btn-info" type="submit" name="button2" value="${theme.name}">${theme.name}</button>
                                            </li>

                                            </c:forEach>
                                        </form>
                                    </ul>
                                </div>
                            </ul>
                        </div>
                    </div>
                </nav>
                    <c:forEach items="${list_courses}" var="course">
                <div class="container">
                    <div class="col-md-12">
                        <div class="card b-1 hover-shadow mb-20">
                            <div class="media card-body">

                                <div class="media-body">
                                    <div class="mb-2 d-flex align-items-center">
                                        <span class="fs-35"> <i class="fas fa-book"></i></span>

                                        <span class="fs-35"><c:out value="${course.description}"/></span>
                                    </div>
                                    <small class="fs-20 fw-300 ls-1"><c:out value="${course.themeName}"/></small>
                                </div>
                                <div class="media-right text-right d-none d-md-block">
                                    <div class="media-right text-right d-none d-md-block">
                                        <div class="media-left pr-12">
                                            <img class="avatar avatar-xl no-radius" src="https://bootdey.com/img/Content/avatar/avatar4.png" alt="...">
                                        </div>
                                        <p class="fs-14 text-fade mb-12"><c:out value="${course.teacherName}"/></p>
                                    </div>
                                    <span class="text-fade"><i class="far fa-clock"></i><c:out value="${course.duration}"/></span>
                                    <span class="text-fade"><i class="fas fa-user-check"></i><c:out value="${course.count}"/></span>
                                </div>

                            </div>

                            <footer class="card-footer flexbox align-items-center">
                                <div>
                                    <fmt:message key="main.start_date" var="start_date"/>
                                    <strong>${start_date}:</strong>
                                    <span> <c:out value="${course.start_date}"/></span>
                                </div>
                                <div class="card-hover-show">
                                    <fmt:message key="main.details_registration" var="details"/>
                                    <a class="btn btn-xs fs-10 btn-bold btn-info" href="course?id=${course.id}">${details}</a>
                                </div>
                            </footer>
                        </div>
                    </div>
                </div>
                    </c:forEach>
                <util:navigation path="listcourses"
                                 maxPage="${maxPage}" page="${page}"/>
            </div>
        </div>
    </div>
</header>

</body>
</html>
