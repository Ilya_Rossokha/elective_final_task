<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<c:choose>
    <c:when test="${locale == 'ru'}">
        <fmt:setLocale value="ru"/>
    </c:when>
    <c:otherwise>
        <fmt:setLocale value="en"/>
    </c:otherwise>
</c:choose>
<fmt:setBundle basename="local"/>
<html>
<head>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Bitter:400,700">
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <link rel="stylesheet" type="text/css" href="../../styles/register.css">
    <link rel='icon' type='image/x-icon' href='../../image/reglog.ico'/>
    <title>Login Page</title>
</head>
<body>

<jsp:include page="header2.jspf"/>
<div class="signup-form">
    <form action="/login" method="post">
        <c:set var="message" value="${requestScope['errorMessage']}"/>
        <c:if test="${not empty message}">
            <p class="errormes">${message}</p>
        </c:if>

        <fmt:message key="login.header" var="head"/>
        <h3>${head}</h3>
        <hr>
        <div class="form-group">
            <div  class="input-group">
                <div class="input-group-prepend">
					<span class="input-group-text">
						<span class="fa fa-user" aria-hidden="true"></span>
					</span>
                </div>
                <fmt:message key="login.login_form.login" var="login"/>
                <input type="text" class="form-control input-lg" name="login" placeholder="${login}" required="required">
                </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-prepend">
					<span class="input-group-text">
						<i class="fa fa-lock"></i>
					</span>
                </div>
                <fmt:message key="login.login_form.password" var="pass"/>
                <input type="password" class="form-control input-lg"  name=password placeholder="${pass}" required="required">
                </div>
        </div>
        <div class="g-recaptcha form-group"
             data-sitekey="6Lf8nGcaAAAAAAZmwcaBPe6APa-nueIdBN7aNwRD">

        </div>
        <fmt:message key="login.login_form.submit" var="log_in"/>
        <button type="submit" class="btn btn-primary btn-block">${log_in}</button>
    </form>
    <fmt:message key="login.login_form_forgot" var="forgot"/>
</div>
</body>
</html>
