<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<c:choose>
    <c:when test="${locale == 'ru'}">
        <fmt:setLocale value="ru"/>
    </c:when>
    <c:otherwise>
        <fmt:setLocale value="en"/>
    </c:otherwise>
</c:choose>
<fmt:setBundle basename="local"/>
<html>
<head>
    <title>Students</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel='icon' type='image/x-icon' href='../../image/students.ico'/>
    <style>
        .main-content{
            position: relative;
            top: 70px;
        }
    </style>
</head>
<body>
<%@include file="header.jspf"%>
<div class="main-content">
    <table class="table table-dark">
        <thead class="thead-light">
        <tr>
            <th scope="col">ID</th>
            <fmt:message key="admin.page_student_firstname" var="first"/>
            <th scope="col">${first}</th>
            <fmt:message key="admin.page_student_surname" var="surn"/>
            <th scope="col">${surn}</th>
            <fmt:message key="admin.page_student_account_id" var="accId"/>
            <th scope="col">${accId}</th>
            <fmt:message key="admin.page_student_action" var="act"/>
            <th scope="col">${act}</th>
        </tr>
        </thead>
        <c:forEach items="${list_students}" var="student">
            <tbody>
            <tr>
                <td><c:out value="${student.id}" /></td>
                <td><c:out value="${student.firstname}" /></td>
                <td><c:out value="${student.surname}" /></td>
                <td><c:out value="${student.account_id}" /></td>
                <form action="/students" method="post">
                    <c:choose>
                        <c:when test="${student.isBlocked == 1}">
                            <fmt:message key="admin.page_student_block" var="blc"/>
                            <fmt:message key="admin.page_student_block_confirmation" var="blconf"/>
                            <td><button class="btn btn-danger" type="submit" name="block" value="<c:out value="${student.account_id}"/>" onclick="return confirm('${blconf}');">${blc}</button></td>
                        </c:when>
                        <c:otherwise>
                            <fmt:message key="admin.page_student_unblock" var="unblc"/>
                            <fmt:message key="admin.page_student_unblock_confirmation" var="unblconf"/>
                            <td><button class="btn btn-success" type="submit" name="unblock" value="<c:out value="${student.account_id}"/>" onclick="return confirm('${unblconf}');">${unblc}</button></td>
                        </c:otherwise>
                    </c:choose>
                </form>
            </tr>
            </tbody>
        </c:forEach>
    </table>
    <util:navigation path="students"
                     maxPage="${maxPage}" page="${page}"/>
</div>
</body>
</html>
