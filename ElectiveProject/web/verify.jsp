<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Verify Page</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" rel="stylesheet">

    <link rel='icon' type='image/x-icon' href='../../image/verify.ico'/>
    <link rel="stylesheet" type="text/css" href="../../styles/register.css">
    <style>
        body{
            background: ivory;
        }
    </style>
</head>
<body>
<c:set var="message" value="${requestScope['errorMessage']}"/>
<c:if test="${not empty message}">
    <p class="errormes">${message}</p>
</c:if>
<div class="signup-form">
    <fmt:message key="verify.page_text" var="tx"/>
    <p>${tx}</p>
    <form action="/verify" method="post">
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <i class="fa fa-lock"></i>
                    </span>
                </div>
                <fmt:message key="verify.page_code" var="cd"/>
                <input class="form-control input-sm" type="text" placeholder="${cd}" name="authcode" required="required">
            </div>
        </div>
        <fmt:message key="verify.page_submit" var="sm"/>
        <button type="submit" class="btn btn-dark">${sm}</button>
    </form>
</div>
</body>
</html>
